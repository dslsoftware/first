//eventHandlerWindows.h
//v2.0 14.01.2017

#include "stdafx.h"
#include "Window.h"
#include "Mouse.h"
#include "Design.h"
#include "Font.h"

#ifndef DSL__EVENT_HANDLER_WINDOWS__H
#define DSL__EVENT_HANDLER_WINDOWS__H

// ��� ��������� ������������ ����������:
// 1. ������ ���������
// 2. ����������� vecWin ������ �� ������ ����
// 3. ���� ������ �������� ������, ����������� set(...);

struct designWindows
{
	std::vector<Window*>*      vecWin;    // ������ �� ������ ����
	std::vector<DesignWindow*> vecDesWin; // ������ �������� ��� ����
	int size;                             // ����� �������� �������
	designWindows() : size(0)
	{
		data();
	}
	void data()
	{
		vecWin = nullptr; // ������ ���� ����
		size = 0;
		vecDesWin.reserve(0);
		//set("images/Design/Window/flyff.png");      
		set("images/Design/Window/Windows8.1.png"); // ����������� ������ ���� - Windows8.1
		//set("images/Design/Window/Windows8.1.2.png");
	}
	// ���������� ������ ����
	void set(sf::String filename)
	{
		int num = checkDesName(filename); // �������� ����� ������� � �������

		if(num >= 0) 
		{ // ������ ������
			if(num != size)// ���� ������ �� �������������
				size = num; // ������������� ������
		} else { // ������ �� ������
			// ���� �������� ������� ������ �������
			if(addDesign(filename))
				size = vecDesWin.size()-1; // ������� ������ - ���������
		}
		// ���� ������� ������ �� ������ ����
		if(vecWin != nullptr)
		{
			// �������� �� ������� ���� � ������� ������ �� �����
		  for(std::vector<Window*>::iterator it = vecWin->begin(); it != vecWin->end(); it++)
		    (*it)->setDesign(vecDesWin[size]);
		}
	}
	// ���������, ���� �� ������ ��� ������ filename � vecDesWin, ���� ���� - ���������� �����, ���� ���, ���������� -1 
	int  checkDesName(sf::String filename)
	{
		// �������� �� ��������
		for(int i=0; i<vecDesWin.size(); i++)
			if(vecDesWin[i]->getFilename() == filename)
				return i;
		return -1;
	}
	// �������� ������ � ������ ��������. ���������� true ���� ������ ��������, � false ���� ������ �� ��������.
	bool addDesign(sf::String filename)
	{
		// ������ ����� ������
		DesignWindow* dw = new DesignWindow();
		// ���� �������� ������ �������
		if(dw->load(filename))
		{
			// ��������� ������ � ������ �������� ����
			vecDesWin.push_back(dw); 
			// ���������� ������� �������
			return true;
		}
		// �������� ������ �� �������
		return false;
	}
	// ���������� ������� ������ ����
	DesignWindow* get() 
	{ 
		return vecDesWin[size];
	}
};

class EventHandlerWindows
{
private:
	Mouse* mouse;
	int x, y; // ���������� ������� ��� ���������� ����� ����
	bool flag0; // true - ��������� �� ����� ����
	bool flag1; // true - ������� � ����� ����
	bool flag2; // true - ������� � ������� �����������
	bool flag3; // true - ������� � ������������� �������
	bool flagx;
	bool flagButton; // true - ������� ����� ������
	int numRote; // ����������� ������������ (behaviorCursor(), behaviorCase1())

	Window* winCur;    // �� ����, �� ������� ������ �����
	Window* winSelect; // �� ���� �� ������� ���� ������ �����

	int numWinActiv; // ���������� �������� ���� 
private:
	void behaviorCase0();
	void behaviorCase1();
	void behaviorCase2();
	void behaviorCase3();
	//behaviorCase0 : ����������
	void resetFlag0();
	void searchWindow();
	void behaviorButton();
	void behaviorCursor();
	void checkModeDrag0();
	//behaviorCase1 : ����������
	bool checkWindow(std::vector<Window*>::iterator);
	//behaviorCase2 : ����������
	bool behaviorButton2();
	void controlWindow();
	void checkModeDrag2();
	//behaviorCase3 : ����������
	void behaviorButton3();
	void behaviorCheckBox();
	void setToZer0();
	//��������� ����������
	void switchModeDrag();
public:
	std::vector<Window*> vecWindow;  // ���� � ������ �����������
	std::vector<Window*> vecWindow2; // ���� � ������� ����������
	bool modeDrag; // ����� �������������� (��� ����������� ������� �� ����� ����, ��������� ���������� ������)
	bool modeDrag2; // 
	///////////////////////////////
	sf::RenderWindow* renderWindow; // ������ �� ����
	designWindows* design; // ��������, ���������� � ���� ��� ������� ����
  ///////////////////////////////
	void data(void);
	static EventHandlerWindows* Instance()
	{
		static EventHandlerWindows* EHW_ = new EventHandlerWindows();
    return EHW_;
	}

	//EventHandlerWindows(void);
	//~EventHandlerWindows(void);

	// ��������� ���� � ���������� ����
	void add(Window* window);
	
	// ���������� ������� �������
	// type: 
	// 0 - nope 
	// 1 - pressed
	// 2 - ...
	// 3 - released
	bool eventKey(int type, int code);
	bool eventMouse(int type, int code);
	void behavior(float time);
	// ���������� ����
	void sort1(int j);

	// ���������� ��������� ���� ��� ������� num � ��������� flag
	bool setVisibleWin(int num, bool flag);
	// �������� �� �������������� �������� ��������� ����
  void switchVisibleWin(int num);

	// ��������� �� �������� ����
	void draw(sf::RenderWindow* window);
	void draw();
};

#endif DSL__EVENT_HANDLER_WINDOWS__H