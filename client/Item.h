//Item.h
//v0.2.1 08.01.2017
#ifndef DSL__ITEM__H
#define DSL__ITEM__H

#include "stdafx.h"
#include "myMath.h"

class Item
{
public:
	sf::Image   image;
	sf::Texture texture;
	sf::Sprite  sprite;
	vec2i size;
	vec2i pos;
public:
	Item(void);
	~Item(void);
	bool loadFromFile(sf::String);
	void setImage(sf::Image);
  void draw(sf::RenderWindow* window);
	vec2u getSize() { return image.getSize(); }
	vec2f getPosition() { return sprite.getPosition(); }
	void setPosition(vec2f pos);
	void setTextureRect(vec4i vec);
	bool check1(int x, int y);
};

#endif DSL__ITEM__H