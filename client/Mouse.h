//Mouse.h
//v2.0 07.01.2017

#ifndef DSL_MOUSE_H
#define DSL_MOUSE_H

#include "stdafx.h"
#include "Cursor.h"

class Mouse
{
private:
	Mouse();  // ����������� ����������
	~Mouse() { } // � ����������
  // ���������� ����� ��������� �����������
  //Mouse( Mouse const&) = delete;
  //Mouse& operator= ( Mouse const&) = delete;

	Cursor* cursor;
  bool mass[5];
	void data();
	sf::RenderWindow* window; // ������ �� ����
public:
	sf::Vector2i pos; // ��������� �� x:y
	int x; // ��������� �� x
	int y; // ��������� �� y
public:
	static Mouse& Instance()
	{
		 static Mouse mouse;
     return mouse;
	}

	void setWindow(sf::RenderWindow *RenderWindow);

	void behavior();

	// ������ ������� ���������� ����� �������, 
	// ������� ��������� � ����������� ��������, 
	// � ������� ������ �������
	// 0 - ��� �������         (�����������)
	// 1 - ������� �������     (��������)
	// 2 - ����������� ������� (�����������)
	// 3 - ���������� �������  (��������)
	int getEventButton(sf::Mouse::Button button);

	void setDesign(sf::String filename);

	void setCursor(int number);

	void draw();

	void setMouseCursorVisible(bool boolean);
};

#endif  DSL_MOUSE_H