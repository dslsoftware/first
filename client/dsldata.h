//dsldata.h

#define VERSION_CUR 3
#define WINDOW_WIDTH_MAX 1000
#define WINDOW_HEIGHT_MAX 650

//���������� winItem � Window.h
#define WINITEM     0
#define WINSPRITE   1
#define WINIMAGE    2
#define WINBUTTON   3
#define WINSLOT     4
#define WINCHECKBOX 5
#define WINTEXTBOX  6
#define WINLABEL    7

// ��� �������� � Window.h
// 0 | 1
// --+--
// 3 | 2
#define LEFT_TOP   0
#define RIGHT_TOP  1
#define RIGHT_DOWN 2
#define LEFT_DOWN  3