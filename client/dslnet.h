//dslnet.h
#include "stdafx.h"
#include "Object1.h"
#include "eventHandlerWindows.h"

const unsigned short PORT = 621;
//const std::string IPADDRESS("192.168.0.103");
const std::string IPADDRESS(sf::IpAddress::getLocalAddress().toString());
sf::IpAddress address;
sf::TcpSocket socket;

struct Client
{
	int id;
	Object1 obj;
	vec2i pos;
	std::string nickname;
	Client()
	{
		id = -1;
		obj.loadFromFile("tileset/mag.png");
		obj.image.setOriginNum(5);
		obj.image.data_frames(3,4);
		obj.image.setScale(2,2);
		obj.setPosition(pos.x, pos.y);
	}
	void set_nickname(std::string str)
	{
		nickname = str;
		obj.set_nickname(str);
	}
};

struct baseClient
{
	Font font;
	int index; // ������� getClient �������� �������
	baseClient() 
	{
		addClient(-1);
	}
	std::vector<Client*> vec;
	// �������� ������� ������� - ������������ ��
	Client* begin()
	{
		if(vec.size() > 0)
			return vec[0];
		else
			return nullptr;
	}
	void setZeroId(int id)
	{
		if(vec.size() > 0)
			vec[0]->id = id;
	}
	Client* getClientNum(int num)
	{
		if(num < vec.size())
			return vec[num];
		return nullptr;
	}
	// �������� ������� �� id
	Client* getClient(int id)
	{
		for(int i=0; i<vec.size(); i++)
			if(vec[i]->id == id)
			{
				index = i;
				return vec[i];
			}
		return nullptr;
	}
	// �������� �������
	Client* addClient(int id)
	{
		if(getClient(id) != nullptr)
			return vec[index];
		// ���� ��� ������ id, ������ ������ �������
		else
		{
			Client* client = new Client();
			client->id = id;
			vec.push_back(client);
			return client;
		}
	}
	void nextpos(int id, vec2i pos1, vec2i pos2)
	{
		Client* client = getClient(id);
		if(client != nullptr)
		{
			client->obj.setPosition(pos1.x, pos1.y);
			client->obj.newTargetMove(1, pos2.x, pos2.y);
		}
	}
	
	// ������� ������� �� ������ �� id
	void connect_client(int id, std::string nickname, sf::Vector2i pos)
	{
		// ���� ������� �� id
		Client* client = getClient(id);
		// ���� ������� ��� � ����
		if(client == nullptr)
		{
			Client* client2 = new Client();
			client2->id = id;
			client2->pos = pos;
			client2->obj.setPosition(pos.x, pos.y);
			//client2->obj.setFont(font);
			client2->set_nickname(nickname);
			vec.push_back(client2);
			
			cout << "connect client "<<nickname<<": true" << endl;
		} else {
			client->pos = pos;
			client->obj.setPosition(pos.x, pos.y);
			client->set_nickname(nickname);
			cout << "connect client " << nickname << ": false" << endl;
		}
	}
	void disconnect_client(int id)
	{
		if(getClient(id) != nullptr)
		{
			vec.erase(vec.begin() + index);
			index = -1;
			cout << "disconnect_clien id=" << id << endl;
		}
	}
	void set_nickname(int id, std::string nickname)
	{
		cout << "set_nickname("<<id<<","<<nickname<<");";
		if(getClient(id) != nullptr)
		{
			vec[index]->set_nickname(nickname);
			cout << "true";
		}
		cout << endl;
	}

	void behavior(float time)
	{
		for(int i=0; i<vec.size(); i++)
			vec[i]->obj.behavior(time);
	}
	void draw(sf::RenderWindow* window)
	{
		for(int i=0; i<vec.size(); i++)
			vec[i]->obj.draw(window);
	}
};

baseClient* base;


void connection(sf::TcpSocket* s)
{
	cout << "connection" << endl;
	while(s->connect(IPADDRESS, PORT) != sf::Socket::Done)
		cout << ".";
	cout << endl << "connection!" << endl;
}

void packets()
{
	while(true)
	{
		sf::Packet packet;
		socket.receive(packet); // ��������� �����
		// ���� ����� �� ������
		if(packet.getDataSize() != 0)
		{
			std::string str;
			sf::Uint16 code;
			packet >> code;
			int id = -1;
			cout << "packet(code=" << code << ")";

			if(code == 0) { }
			else if(code == 2) str = "����� �����.";
			else if(code == 3) str = "����������� �������.";
			// ����� �� �����������
			else if(code == 11) {
				packet >> id;
				str = "����������� �������: id="  + intToString(id);
				base->setZeroId(id);
			}
			else if(code == 12) str = "�����������: ������ ������.";
			else if(code == 13) str = "�����������: ������ ������.";
			else if(code == 14) str = "�����������: ������ ������.";
			// ����� �� �������� ���������� ����������
			else if(code == 21) {} //str = "Logout: true";
			else if(code == 22) { } //str = "Logout: false";
			//
			else if(code == 31)
			{
				std::string nickname;
				packet >> id >> nickname;
				str = "����� ���: " + nickname;
				base->set_nickname(id, nickname);
			}
			// ����� �� �������������� ����������
			else if(code == 41) cout << "dissconnect true" << endl;
			else if(code == 42) cout << "dissconnect false (no client)." << endl;
			else if(code == 43) cout << "dissconnect false (no rights)." << endl;
			// ��������� ����������� � ����� ������� ��� id
			else if(code == 48) {
				std::string nickname;
				sf::Vector2i vec;
				packet >> id >> nickname >> vec.x >> vec.y;
				base->connect_client(id, nickname, vec);
			// ��������� ����������� �� ���������� ������� ��� id
			} else if(code == 49) {
				packet >> id;
				base->disconnect_client(id);
			// ���������� ���� ������ ��������
			} else if(code == 50) {
				int count = 0;
				vec2i pos;
				//std::string nickname;
				packet >> count;
				for(int i=0; i<count; i++)
				{ 
					std::string nickname;
					packet >> id >> nickname >> pos.x >> pos.y;
					base->connect_client(id, nickname, pos);
					//nickname = "";
				}
			} else if(code == 52) {
				vec2i pos1;
				vec2i pos2;
				packet >> id >> pos1.x >> pos1.y >> pos2.x >> pos2.y;
				// �������� ������� �� id
				Client* client = base->getClient(id);
				if(client != nullptr)
				{
					client->pos = pos1;
					client->obj.setPosition(pos1.x, pos1.y);
					client->obj.newTargetMove(1, pos2.x, pos2.y);
				}
			} else if(code = 61) {
				std::string message;
				packet >> id >> message;
				EventHandlerWindows* ehw = EventHandlerWindows::Instance();
				ehw->vecWindow2[2]->nextMessage("[" + base->getClient(id)->nickname + "] " + message);
			}

			if(str != "")
			{
				EventHandlerWindows::Instance()->vecWindow2[0]->vecLabel[0]->setString(str);
				str = "";
			}
		} 
	}
}

void sendPacket(sf::Packet p) { socket.send(p); }

sf::Packet getPacket(sf::Uint16 code)
{
	sf::Packet packet;
	packet << code;
	return packet;
}
sf::Packet getPacket(sf::Uint16 code, int id)
{
	return getPacket(code) << id;
}
sf::Packet getPacket(sf::Uint16 code, int id, std::string str1)
{
	return getPacket(code, id) << str1;
}
sf::Packet getPacket(sf::Uint16 code, int id, std::string str1, std::string str2)
{
	return getPacket(code, id, str1) << str2; 
}
sf::Packet getPacket(sf::Uint16 code, std::string str)
{
	return getPacket(code) << str;
}
sf::Packet getPacket(sf::Uint16 code, std::string str1, std::string str2)
{
	return getPacket(code, str1) << str2;
}
sf::Packet getPacket(sf::Uint16 code, sf::Vector2i pos1, sf::Vector2i pos2)
{
	return getPacket(code) << pos1.x << pos1.y << pos2.x << pos2.y;
}

void registration(std::string login, std::string password) 
{ 
	sendPacket(getPacket(1, base->getClientNum(0)->id, login, password)); 
}
void login(std::string login, std::string password) 
{ 
	sendPacket(getPacket(10, base->getClientNum(0)->id, login, password)); 
}
void set_nickname(std::string nickname)
{
	sendPacket(getPacket(30, base->getClientNum(0)->id, nickname));
}
void logout() 
{ 
	if(base->getClientNum(0)->id != -1)
		sendPacket(getPacket(20, base->getClientNum(0)->id)); 
}
void newpos(sf::Vector2i pos1, sf::Vector2i pos2)
{ 
	sendPacket(getPacket(51, pos1, pos2)); 
}
void message(std::string message)
{
	sendPacket(getPacket(60, message));
}
void message2(std::string nickname, std::string message)
{
	sendPacket(getPacket(62, nickname, message));
}