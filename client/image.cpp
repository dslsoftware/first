//Image.cpp
//v2.0 07.01.2017

#include "image.h"

Image::Image() { data(); }
Image::Image(sf::String filename)
{
	data();
  loadFromFile(filename);
}
Image::~Image() { }

void Image::data()
{
	isVisible = true;
	x = 0;
	y = 0;
	w = 0;
	h = 0;
	rot = 0;
	setAlpha(255);
}
bool Image::loadFromFile(sf::String filename)
{
  if(!image.loadFromFile("images/" + filename))
		return false;

	texture.loadFromImage(image);
  sprite.setTexture(texture);
	sprite.setTextureRect(sf::IntRect(0, 0, getSize().x, getSize().y)); // ����� ������� ���� ������������� ��� ������
  
	setOriginNum(5); // �������� �� ��������

	w = image.getSize().x;
	h = image.getSize().y;

	return true;
}
void Image::draw(sf::RenderWindow *window)
{
	if(isVisible)
	{
    window->draw(sprite); 
	  outline.draw(window);
	}
}
void Image::update()
{
	// ���������� Outline
	outline.setRotation(getRotation());
  outline.setPosition(getPosition());
}


void Image::setX(float x)
{
	this->x = x;
	sprite.setPosition(x, y);
}
void Image::setY(float y)
{
	this->y = y;
	sprite.setPosition(x, y);
}
void Image::setPosition(float x, float y)
{
	this->x = x;
	this->y = y;
	sprite.setPosition(x, y);
}
void Image::setPosition(sf::Vector2f position)
{
	x = position.x;
	y = position.y;
	sprite.setPosition(position);
}
void Image::move(float dx, float dy)
{
	x += dx;
	y += dy;
	sprite.move(dx, dy);
}
void Image::move2(float distance, float angle)
{
	float dx =      distance * mySin(angle);
  float dy = -1 * distance * myCos(angle);
  move(dx, dy);
}

void Image::update_alpha()
{
  if(alpha < 0)   alpha = 0;
  if(alpha > 255) alpha = 255;
  sprite.setColor(sf::Color(255,255,255,alpha));
}
void Image::setAlpha(float value_alpha)
{
  alpha = value_alpha;
  update_alpha();
}
void Image::setDAlpha(float value_alpha)
{
  alpha += value_alpha;
  update_alpha();
}

void Image::setRotation(float angle)
{
	rot = angle;
  sprite.setRotation(angle);
}
void Image::rotate(float angle)
{
	rot += angle;
  sprite.rotate(angle);
}

void Image::setScale(float factorX, float factorY)
{
	sprite.setScale(factorX, factorY);
}
void Image::setScale(sf::Vector2f scale)
{
	sprite.setScale(scale);
}
void Image::scale(float factorX, float factorY)
{
	sprite.scale(factorX, factorY);
}
void Image::scale(sf::Vector2f scale)
{
	sprite.scale(scale);
}

void Image::setOrigin(float x, float y)
{
	sprite.setOrigin(x, y);
}
void Image::setOriginNum(int number)
{
	int width  = image.getSize().x;
	int height = image.getSize().y;
	originNum = number;
	switch(number)
	{
	case 1: setOrigin(             0,               0); break;
	case 2: setOrigin(float(width)/2,               0); break;
	case 3: setOrigin(         width,               0); break;
	case 4: setOrigin(             0, float(height)/2); break;
	case 5: setOrigin(float(width)/2, float(height)/2); break;
	case 6: setOrigin(         width, float(height)/2); break;
	case 7: setOrigin(             0,          height); break;
	case 8: setOrigin(float(width)/2,          height); break;
	case 9: setOrigin(         width,          height); break;
	default:
		std::cout << "Warning: Image::setOriginNum(1~9); originNum = -1;" << std::endl;
		originNum = -1;
	}
}

void Image::flipHorizontally()
{
	image.flipHorizontally();
	texture.loadFromImage(image);
  sprite.setTexture(texture);
}
void Image::flipVertically()
{
	image.flipVertically();
	texture.loadFromImage(image);
  sprite.setTexture(texture);
}

// ������������ ��� ��������� ����������� ������� �� ��������������
void Image::setImage(sf::Image image)
{
	this->image = image;
	texture.loadFromImage(image);
  sprite.setTexture(texture);
	sprite.setTextureRect(sf::IntRect(0, 0, getSize().x, getSize().y)); // ����� ������� ���� ������������� ��� ������
}

void Image::setPixel(unsigned int x, unsigned int y, const sf::Color color)
{
	image.setPixel(x, y, color);
}
sf::Color Image::getPixel(unsigned int x, unsigned int y) const
{
	if(x<0 || y<0 || x>=w || y>=h)
		return sf::Color(0,0,0,0);
	else
	  return image.getPixel(x, y);
}
int Image::getAlphaPixel(unsigned int x, unsigned int y) const
{
	if(x <  0) { std::cout << "Error: Image::getAlphaPixel("<<x<<","<<y<<") x<0" ; }
	if(w <= x) { std::cout << "Error: Image::getAlphaPixel("<<x<<","<<y<<") w<=x"; }
	if(y <  0) { std::cout << "Error: Image::getAlphaPixel("<<x<<","<<y<<") y<0" ; }
	if(h <= y) { std::cout << "Error: Image::getAlphaPixel("<<x<<","<<y<<") h<=y"; }
	return image.getPixel(x, y).a;
}

//