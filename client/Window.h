//Window.h
//v2.0 07.01.2017

#ifndef DSL__WINDOW__H
#define DSL__WINDOW__H

#include "stdafx.h"
#include "myMath.h"
#include "Item.h"
#include "Design.h"
#include "Mouse.h"
#include "mcText.h"
#include "dsldata.h"
#include "textBox.h"

struct winItem;
struct winSprite;
struct winImage;
struct winButton;
struct winSlot;
struct winCheckBox;
struct winTextBox;
struct winLabel;

struct winMcText;
struct winSave;

class Window
{
public:
	//12.04.2017 13:42
	winItem* curItem;    // ������� �� ������� ������
	winItem* itemSelect; // ������� �� ������� ������ ���������

	vec2i pos;     // ����������
	vec2i size;    // ������
	vec2i sizeMin; // �����������  ������
	vec2i sizeMax; // ������������ ������
	vec4i border;  // ������� left top right down

	bool isVisible;
	bool isMove;           // ����� ���������� ����?
	bool isConsiderBorder; // ��������� ������� ���� ���������?
	bool isChangeSize;     // ����� �������� �������?
	bool isTarget;     // ����� �� ����?
	bool flagMoves;    // ���� �����������. ���� ���� ���� ����������� ��� ������� ������, �� true (������������ � winMcText) 

	DesignWindow* designWindow; // ������ �� ������ ����

	std::vector<winItem*> vecObj;
	std::vector<winItem*> vecSprite;
	std::vector<winItem*> vecImage;
	std::vector<winItem*> vecButton;
	std::vector<winItem*> vecSlot;
	std::vector<winItem*> vecCheckBox;
	std::vector<winItem*> vecTextBox;
	std::vector<winItem*> vecLabel;

	winSave* wSave; // ������� ����, �������� ���� � ���� �� ���������

	sf::RenderWindow* renderWindow;
	mcText* text;
	Mouse* mouse;
	sf::Font* font;
public:
	void data();
	Window(void);
	Window(int width, int height);
	Window(int width, int height, bool isMove, bool isConsiderBorder, bool isChangeSize);
	~Window(void);

	//12.04.2017 13:40
	// type - [pressed/released] code - [numKey]
	bool eventKey(int type, int code);
	// type - [pressed/released] code - [numKey]
	void eventMouse(int type, int code);

	void resetSelect();
	int  behaviorButton(int code);
	void behaviorCursor();

	void behavior(float time);
	//=======/\=======\\
	//======//\\======\\
	//=====//  \\=====\\
	//====//    \\====\\


	void setText(sf::String str);
	void save(vec2i, int);
	void draw(sf::RenderWindow*);   // ���������
	//////////////////////////////////////
	// ���������� ���������
	//////////////////////////////////////
	void setDesign(DesignWindow* desWin);
	void update();
	//////////////////////////////////////
	// ������������� ������
	//////////////////////////////////////
	void switchVisible()        { isVisible        = !isVisible;        }
	void switchChangeSize()     { isChangeSize     = !isChangeSize;     }
	void switchMove()           { isMove           = !isMove;           }
	void switchConsiderBorder() { isConsiderBorder = !isConsiderBorder; }
	//////////////////////////////////////
	// �������
	//////////////////////////////////////
	void setSize(int, int); // ��������� ������ ����
	void setSizeMax(int width, int height) { sizeMax = vec2i(width, height); }
	void setSizeMin(int width, int height) { sizeMin = vec2i(width, height); }
	//////////////////////////////////////
	// ���������� ��� ���������� �����
	///////////////////////////////////////
	void setPosition(int x, int y); // ���������� ����������
	void changeSize();							// ��������� ��������
	void changePosition();					// ��������� ���������
	////////////////////////////////////////
	// ��������
	////////////////////////////////////////
	bool check1(int x, int y); // ������ �� � ����
	bool check2(int x, int y); // ������ �� � ������� �����, �� ������� �������������
	int  check3(int x, int y); // ������ �� ������ � ���� ��� ������������
	int  checkButton(int x, int y); // ������ �� ������ � ������ (���������� ����� ������)
	int  checkCheckBox(int x, int y); // ������ �� � checkBox-� (-1 ���� ���)
	int  checkTextBox(int x, int y);
	/////////////////////////////////////////
	// ���������� ���������
	/////////////////////////////////////////
	void addItem(int type, sf::String filename, vec2i offset, int code, int numTiles);
	void addItem(int type, sf::Image image, vec2i offset, int code, int numTiles);
	///////////////////////////////
	void nextMessage(std::string message);
};


struct winItem
{
	void (*func)(std::string login, std::string password);
	void (*func2)(std::string str);
	int type; // ��� (�� dsldata)
	Window* win; // ������ �� ����
	Item* item;  // ������ �� item

	vec2i offset; // �������� ������������ ����
	vec2i size;   // ������
	vec2i pos;    // �������

	int code;     // ����� �������� (0 - ����� ����)
	int num;      // ����� �������� �����

	bool flag1;     // ��������... (true - �������������� �������)
	bool isLocked;  // ����������� (true - ������������)
	//========================================================
	winItem() { data(); }
	// �������� ��� ����������
	void data()
	{
		func  = nullptr;
		func2 = nullptr;
		type = -1;
		win = nullptr;
		item = nullptr;
		pos = vec2i(0,0);
		size = vec2i(0,0);
		set(0, vec2i(0,0));
		flag1 = false;
		isLocked = false;
		num = -1;
	}
	// ���������� �������� � ��������
	void set(int code, vec2i offset)
	{
		this->code = code;
		this->offset = offset;
	}
	// [winMcText] �������� ���������
	virtual vec2i update()
	{
		vec2i newPos;
		// ���� ���� ����
		if(win != nullptr)
		{
			newPos = win->pos; //���������� ����������  ����

			// ���� �������� �����
			if(code == 0 || code == 3) 
				newPos.x += offset.x; 
			// ����� �������� ������
			else	
				newPos.x += win->size.x - offset.x;

			// ���� �������� ������
			if(code == 0 || code == 1)
				newPos.y += offset.y;
			// ����� �������� �����
			else
				newPos.y += win->size.y - offset.y;
		}
		pos = newPos;
		if(item != nullptr)
			item->setPosition(vec2f(pos.x, pos.y));
		return pos;
	}
	// �������� �� ���������� ��������� � item
	virtual bool check1(int  x, int  y) 
	{ 
		if(item != nullptr) 
			return item->check1(x, y); 
		else 
			return false; 
	}
	// ���������� ����
	void setWindow(Window* w)	{ win = w; }
	// ������������� �� ��������� ����� � ���� ��������
	virtual int  init(sf::String filename, int code)
	{
		// code nope
		data();
		item = new Item();
		if(item->loadFromFile(filename))
		{
			size = item->size;
			// ���� ���������
			if(code != 0 && code != 1)
			{
				if(code > 0) flag1 = true;
				code = myMod(code);
			
				if(flag1 == false) size.y = size.y / code; // ��� ������������
							        else size.x = size.x / code; // ��� ��������������

				setTile(0);	
			}
			return 0;
		}
		return 1;
	}
	//========================================================
	// [winMcText] ���������
	virtual void draw(sf::RenderWindow* window) { if(item) item->draw(window); }
	// [winCheckBox] ����������� �����
	virtual bool switchFlag() { return false; }
	// [winButton] ����������� �����������
	virtual bool switchLocked() { isLocked = !isLocked; return isLocked; }
	// [winButton, winCheckBox] ���������� ����
	virtual void setTile(int number) 
	{ 
		if(item != nullptr && !isLocked && number != num)
		{
			num = number;
			if(!flag1) item->setTextureRect(vec4i(0, size.y * num, size.x, size.y)); // ������������
			      else item->setTextureRect(vec4i(size.x * num, 0, size.x, size.y)); // ��������������
		}
	} 
	// [winSprite] ������������� �� �������� � ����
	virtual int  init(sf::Image image, int code) { return 0; }
	// [winSprite] ��������� �������� item->sprite
	virtual void setScale(int factorX, int factorY) { }
  // [winTextBox] �������� ��������
	virtual bool eventKey(int type, int code)	{ return false; }
	virtual void eventMouse(int type, int code) { }
	virtual void behavior(float time) { }
	virtual void setFont(sf::Font* font) { }
	virtual void setSizeX(int factorX) { }

	void setFunction(void (*f)(std::string login, std::string password)) { func = f; }
	void setFunction2(void (*f)(std::string str)) { func2 = f; }

	void beginFunc(std::string login, std::string password)
	{
		if(func != nullptr)
			func(login, password);
	}
	void beginFunc2(std::string str)
	{
		if(func2 != nullptr)
			func2(str);
	}


	virtual void setString(std::string str) { }
	virtual std::string getString() { return NULL; }
	virtual void setMaxSize(int maxsize) { }
};
struct winSprite : winItem
{
	sf::Image image0;
	winSprite() { type = WINSPRITE; }
	int init(sf::Image image, int code)
	{
		item = new Item();
		if(setImage(image, true))
			return 0;
		return 1;
	}
	bool setImage(sf::Image img, bool flag)
	{
		if(flag) image0 = img;
		if(item != nullptr)
		{
			item->setImage(img);
			size = item->size;
			return true;
		}
		return false;
	}
	void setScale(int factorX, int factorY)
	{
		bool bx = factorX > 1;
		bool by = factorY > 1;
	  if((bx && !by) ||(!bx && by))
		{ // ���� ���������� ������ � 1 �������
		  item->sprite.setScale(factorX, factorY);
			size.x = image0.getSize().x * factorX;
		  size.y = image0.getSize().y * factorY;
		} 
		else if(bx && by)
		{ // ���� ��������� �������� � ��� �������
			sf::Image img;
			img.create(image0.getSize().x, factorY, sf::Color::Red);
			img.copy(item->image, 0, 0, vec4i(0,0,size.x,size.y)); // �������� � �� ��� ��������� image
			if(factorY > size.y)
			{
				for(int i = size.y; i<factorY; i += image0.getSize().y)
					img.copy(image0, 0, i, vec4i(0, 0, image0.getSize().x, image0.getSize().y));
			}
			setImage(img, false);
			setScale(factorX, 1);
		}
	}
};
struct winImage : winItem
{
	winImage() { type = WINIMAGE; }
};
struct winButton : winItem
{
	winButton() { }
	bool switchLocked() 
	{ 
		if(isLocked) setTile(0);
		        else setTile(3);
		isLocked = !isLocked; 
		return isLocked;
	}
};
struct winSlot : winItem
{
	winSlot() { type = WINSLOT; }
};
struct winCheckBox : winItem
{
	bool flag2; // CheckBox
	winCheckBox() { type = WINCHECKBOX; flag2 = true; }
	bool switchFlag()
	{
		flag2 = !flag2;
		if(flag2) setTile(1);
		     else setTile(0);
		return flag2;
	}	
};
struct winMcText : winItem
{
	mcText* text;
	winMcText() { text = new mcText(); }
	void draw(sf::RenderWindow* window)
	{
		if(win->flagMoves)
			text->setPosition(update());
		text->draw(window);
	}
};
struct winTextBox : winItem
{
	textBox* tb;
	winTextBox(int w, int h) 
	{ 
		tb = new textBox(w, h); 
	}
	vec2i update()
	{
		vec2i newPos;
		// ���� ���� ����
		if(win != nullptr)
		{
			newPos = win->pos; //���������� ����������  ����

			// ���� �������� �����
			if(code == 0 || code == 3) 
				newPos.x += offset.x; 
			// ����� �������� ������
			else	
				newPos.x += win->size.x - offset.x;

			// ���� �������� ������
			if(code == 0 || code == 1)
				newPos.y += offset.y;
			// ����� �������� �����
			else
				newPos.y += win->size.y - offset.y;
		}
		pos = newPos;

		tb->setPosition(pos.x, pos.y);

		return pos;
	}
	void draw(sf::RenderWindow* window) { tb->draw(window); }
	bool eventKey(int type, int code) { return tb->eventKey(type, code); }
	void eventMouse(int type, int code) { tb->eventMouse(type, code);	}
	void behavior(float time) { tb->behavior(time);	}
	bool check1(int  x, int  y) { return tb->check1(vec2f(x, y)); }
	void setString(std::string str) { tb->text.setString(""); }
	std::string getString() { return tb->text.getString(); }
	int  init(sf::String filename, int code)
	{
		DesignTextBox* des = new DesignTextBox(filename);
		tb->x1 = new imgx(des->getImage(0));
		tb->x2 = new imgx(des->getImage(1));
		tb->x3 = new imgx(des->getImage(2));
		setSizeX(code);
		return 1;
	}
	void setScale(int factorX, int factorY)	{ tb->x2->sprite.setScale(factorX, factorY); }
	void setSizeX(int factorX)
	{
		if(factorX < 20) factorX = 20;
		int x1 = tb->x1->image.getSize().x; // ������ ��������
		int x2 = factorX - 2*x1;     // ������ �������������
		tb->x2->sprite.setScale(x2, 1);
		vec2f pos = tb->x1->sprite.getPosition();
		pos.x += x1; tb->x2->sprite.setPosition(pos);
		pos.x += x2; tb->x3->sprite.setPosition(pos);
	}
	virtual void setMaxSize(int maxsize) { tb->maxsize = maxsize; }
};
struct winLabel : winItem
{
	Label* l;
	winLabel() { l = new Label(); }
	void setString(std::string str) { l->setString(str); }
	std::string getString() { return l->getString(); }
	void draw(sf::RenderWindow* window) { l->draw(window); }
	virtual vec2i update()
	{
		vec2i newPos;
		// ���� ���� ����
		if(win != nullptr)
		{
			newPos = win->pos; //���������� ����������  ����

			// ���� �������� �����
			if(code == 0 || code == 3) 
				newPos.x += offset.x; 
			// ����� �������� ������
			else	
				newPos.x += win->size.x - offset.x;

			// ���� �������� ������
			if(code == 0 || code == 1)
				newPos.y += offset.y;
			// ����� �������� �����
			else
				newPos.y += win->size.y - offset.y;
		}
		pos = newPos;
		l->setPosition(vec2f(pos.x, pos.y));
		return pos;
	}
};

struct winSave
{
	vec2i winSize;
	vec2i winPos;
	vec2i mousePos;
	int numRot; // ����������� ������������
	winSave()
	{
		winSize  = vec2i(0,0);
		winPos   = vec2i(0,0);
		mousePos = vec2i(0,0);
		numRot = -1;
	}
};

#endif DSL__WINDOW__H