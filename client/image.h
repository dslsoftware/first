//Image.h
//v2.0 07.01.2017

#include "stdafx.h"
#include "myMath.h"

#ifndef DSL_IMAGE_H
#define DSL_IMAGE_H

struct Rect : sf::RectangleShape
{
	bool isVisible;
	Rect()
	{
		setOutlineThickness(1);
		setOutlineColor(sf::Color::Red);
	  setFillColor(sf::Color(0,0,0));

		isVisible = true;
	}
};

// ��� ������������ ����������?
// 1. � ������� setSize() ������ ������� ��������
// 2. ��� ����������� ����������� ����������
// - setRotation() - ���� �������� ��������(���� ��� ���������)
// - setPosition() - ���������� ��������
// 3. isVisible = true;
struct Outline
{
	bool isVisible;
	Rect rect1; // ������������� ������������� �������� (��� �������� ��������: ���������)
	Rect rect2; // ������������� ������������� �������� (��� �������� ��������: �� ���������, �������� �������)
	Outline()
	{
		isVisible = false;
	}
	void draw(sf::RenderWindow *window)
	{
		if(isVisible)
		{
		  if(rect1.isVisible) window->draw(rect1);
		  if(rect2.isVisible) window->draw(rect2);
		}
	}
	void setPosition(sf::Vector2f position)
	{
		rect1.setPosition(position);
		rect2.setPosition(position);
	}
	void setRotation(float rotation)
	{
		rect1.setRotation(rotation);
		int w = myMod(mySin(rotation)*rect1.getSize().y) + 
						myMod(myCos(rotation)*rect1.getSize().x);
		int h = myMod(myCos(rotation)*rect1.getSize().y) + 
						myMod(mySin(rotation)*rect1.getSize().x);
		rect2.setSize(sf::Vector2f(w, h));
		rect2.setOrigin(w/2 , h/2);
	}
	void setSize(sf::Vector2f size)
	{
		rect1.setSize(size);
		rect1.setOrigin(size.x/2, size.y/2);
	}
};

class Image
{
protected:
  float alpha;         // ������������
  int originNum;       // ����� ��������, ���� -1, �� �� ����������� ��������
	                     // ���� ����� �����������, �� �������� �� ����������� ������������

  sf::String  file;    // ���� � �����������
  sf::Image	  image;   // SFML �����������
  sf::Texture texture; // SFML ��������
  sf::Sprite  sprite;  // SFML ������

  void update_alpha(void); 

public:
	Outline outline; // �������
	bool isVisible;  // ����� �������� (false - ��������� �� �����)
	float x;   // �������� ����������, ������ ��� �������� � �������� ��������� ������
	float y;   // ������: image->getPosition().y ��� image->ys 
	float rot; //         image->getRotation()   ��� image->rot
	int w;     //
	int h;     //
	///////////////////////////////
	///////////////////////////////

  Image(void);
  Image(sf::String filename);
	~Image();

  void data(void);
  bool loadFromFile(sf::String filename);
  void draw(sf::RenderWindow *window);
	void update(void);

	////////////////////
	// ����������
	////////////////////
  void setX(float x);
  void setY(float y);
	void setPosition(float x, float y);
	void setPosition(sf::Vector2f position);
  void move(float dx, float dy);
	void move2(float distance, float angle);
	sf::Vector2f getPosition(void){ return sprite.getPosition(); }
	void coutPosition(void){ std::cout << "Image x:y = " << getPosition().x << ":" << getPosition().y << std::endl; }
	////////////////////

	////////////////////
	// ������������
	////////////////////
  void setAlpha(float value_alpha);
  void setDAlpha(float value_alpha);
	float getAlpha(void){ return alpha; }
	void coutAlpha(void){ std::cout << "Image alpha = " << alpha << std::endl; }
	////////////////////

	////////////////////
	// ���� ��������
	////////////////////
  void setRotation(float angle);
  void rotate(float angle);
	float getRotation(void){ return sprite.getRotation(); }
	void coutRotation(void){ std::cout << "Image rotation = " << rot << std::endl; }
	////////////////////

	////////////////////
	// �������
	////////////////////
  void setScale(float factorX, float factorY);
	void setScale(sf::Vector2f scale);
	void scale(float factorX, float factorY);
	void scale(sf::Vector2f scale);
	sf::Vector2f getScale(void){ return sprite.getScale(); }
	void coutScale(void){ std::cout << "Image scaleX:scaleY = " << getScale().x << ":" << getScale().y << std::endl; }
	sf::Vector2f getSize(void){ return sf::Vector2f(image.getSize().x, image.getSize().y); }
	void coutSize(void){ std::cout << "Image sizeX:sizeY = " << getSize().x << ":" << getSize().y << std::endl; }
	////////////////////

	////////////////////
	// ORIGIN
	////////////////////
	void setOrigin(float x, float y);
	void setOrigin(sf::Vector2f origin);
	void setOriginNum(int number);
	sf::Vector2f getOrigin(void){ return sprite.getOrigin(); }
	void coutOrigin(void){ std::cout << "Image originX:originY = " << getOrigin().x << ":" << getOrigin().y << std::endl; }
	////////////////////

	////////////////////
	// ������
	////////////////////
	sf::String  getFilename(void){ return file;    }
	sf::Image   getImage(void)   { return image;   }
	sf::Texture getTexture(void) { return texture; }
  sf::Sprite  getSprite(void)  { return sprite;  }
	////////////////////

  void setImage(sf::Image image);

	////////////////////
	// 
	////////////////////
  void flipHorizontally(void);
  void flipVertically(void);
	////////////////////

	////////////////////
	// ���������� ���������
	////////////////////
	void setPixel(unsigned int x, unsigned int y, const sf::Color color);
	sf::Color getPixel(unsigned int x, unsigned int y) const;
	int getAlphaPixel(unsigned int x, unsigned int y) const;
	////////////////////
};

#endif DSL_IMAGE_H

//
