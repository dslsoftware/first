#pragma once

#include "stdafx.h"

class Sprite : public sf::Sprite
{
public:
	Sprite() 
	{ 

	}
	
	Sprite(std::string &filename) 
	{ 
		loadFromFile(filename);
	}
	
	bool loadFromFile(std::string &filename)
	{
		if(image.loadFromFile(filename))
		{
			texture.loadFromImage(image);
			setTexture(texture);
			return true;
		}
		return false;
	}

	

	//members
	sf::Image   image;     //
	sf::Texture texture;   //
	float       alpha;     // прозрачность
    int         originNum; //
};

