//Tilemovi.cpp
//v2.0 07.01.2017
#include "Tilemovi.h"

Tilemovi::Tilemovi(void)
{
	data();
}
Tilemovi::Tilemovi(sf::String filename)
{
	data();
	loadFromFile(filename);
}
Tilemovi::~Tilemovi(void)
{
	//
}
// ����� ��������� ����������: ���������� ����������, 4 ����� � �������
void Tilemovi::data(void)
{
	data_number_iteration(-1);
	data_time_frame(0.25); // ������� ����� ������
}
// ����� ������
bool Tilemovi::shift_frame(float time)
{
  ///////////////////////////////////////////////////////////
  // if(numFrame <= 1) - ���� ���� <= 1, �� ������ ����� �� ���� 
  ///////////////////////////////////////////////////////////

  // ���� ���� ���������
	if(numIterationMax != 0)
	{
		// ���� ���������� ����������
		if(numIterationMax < 0)
		{
			// ������ �����
			function1(time);
			return true;
		} 
		// ����� ���� ���������� ����������
		else if(numIterationMax > 0)
		{
			// ���� ���������� �� �����������
			if(numIterationCur < numIterationMax)
			{
				function1(time); 
				return true;
			}
			else
			{
				// ��������� ���������
				//isVisible = false; // ���������� � ����������� ��������� ��� ������ � ������ �����
				numIterationMax = 0;
				//return false;
			}
		}
	}
	return false;
}
// ������� ����� ������
bool Tilemovi::function1(float time)
{
	// ���� ��������� ������ ������ 1
	if(numFrame > 1)
  {
		timeFrameCur += time;
		if(timeFrameCur >= timeFrameMax)
		{
			if(!update_frame())
			{
				// ���� �������� Tileset
				std::cout << "xxx: "<<numIterationCur<<":"<<numIterationMax<<std::endl;
				numIterationCur++;
				return false;
			}
			timeFrameCur -= timeFrameMax;
		}
  }
	return true;
}
// ����� �����, ��� ������� ����� ��������� ����
void Tilemovi::data_time_frame(float time_shift_frame)
{
  timeFrameCur = 0;                //
  timeFrameMax = time_shift_frame; //
}
// ������ ���������� ���������� (������������� - ����������)
void Tilemovi::data_number_iteration(int numIteration)
{
	numIterationMax = numIteration; 
	numIterationCur = 0; 
}

void Tilemovi::play()
{
	//timeFrameCur = 0;
	//numIterationCur = 0;
	//setNumFrameWcur(1);
}
