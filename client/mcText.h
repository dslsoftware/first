#pragma once
#include "stdafx.h"
#include "Font.h"
/////////////////////////////////
// multiline colored text
// mcText
///////////////////////////////

struct img
{
	sf::Image   image;
	sf::Texture texture;
	sf::Sprite  sprite;
	img()
	{
		image.create(1, 16);
		texture.loadFromImage(image);
		sprite.setTexture(texture);
	}
	void set(sf::Vector2f pos, int w)
	{
		sprite.setScale(w, 1);
		sprite.setPosition(pos);
	}
};

// ��������� ���������� ���������� � ������
struct mcTextInfo
{
	sf::String str;  // ������
	sf::Color color; // ����
	int w;  // ������ ������
	int h;  // ������ ������
	int dx; // �������� ������ �� X
	int dy; // �������� ������ �� Y 
	mcTextInfo()
	{
		data();
	}
	void data()
	{
		w = 0;
		h = 0;
		dx = 0;
		dy = 0;
		color = sf::Color::Black;
	}
};

class mcText
{
public:
	img imgPipe;
	bool isLimitW; // ����� �� ��������� ����������� �� ������
	int dx; // ��������� � ��������� �������������
	int dy; // ��������� � ��������� �������������
	int limitW; // ����������� �� ������
	sf::Vector2i index; // x - index ����������� �������
	int charSize;
	std::vector<mcTextInfo*> vecInfo; // ������ ���������� �������� �������
	std::vector<sf::Text> vecText;    // ������ ���������� ���� ������
	sf::Text* end;
	void data(); // ����� ��������� ����������
public:
	mcText();
	~mcText(void);

	int findCharacterSize(sf::Text text, int index) const;
	//int 
	
	sf::Vector2i scan(sf::Vector2i mousePos);
	void setLimit(bool flag, int num);
	
	void add0(int index, sf::String str);
	void add(sf::String str);
	void add(sf::String str, sf::Color color);
	void add2(sf::String str);
	
	void enter();
	void backspace();
	int  backspace(int index);
	void space();
	void space(int value);
	void spaceY(int value);

	void move(float dx, float dy);
	void setPosition(float x, float y);
	void setPosition(sf::Vector2f pos);
	void setPosition(sf::Vector2i pos);
	
	void draw(sf::RenderWindow* window);
	std::vector<sf::String> scanWords(sf::String str);
	std::vector<sf::String> scanString(int w, std::vector<sf::String> vecWords_, sf::Text* text);
	void clear();
	int getW(int num);
};