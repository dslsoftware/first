//Mouse.cpp
//v2.0 07.01.2017
#include "Mouse.h"

Mouse::Mouse()
{
	data();
}

void Mouse::data()
{
	cursor = new Cursor();

	x = 0;
	y = 0;
	pos = sf::Vector2i(0, 0);

	for(int i = 0; i<5; i++)
		mass[i] = false; 
}

void Mouse::setWindow(sf::RenderWindow *RenderWindow)
{
	window = RenderWindow;
}

void Mouse::behavior()
{
	sf::Vector2i pixelPos = sf::Mouse::getPosition(*window);
	sf::Vector2f position = (*window).mapPixelToCoords(pixelPos);
	pos = sf::Vector2i(position.x, position.y);
	x = pos.x;
	y = pos.y;
	cursor->setPosition(x, y);
}

int Mouse::getEventButton(sf::Mouse::Button button)
{
	bool isPressed = sf::Mouse::isButtonPressed(button); // ������ �� ����������� �������?
	bool isPressed2 = mass[button]; // ���� �� �� ����� ������ ������ �������?

	if(isPressed2 != isPressed)
	{ // ���� ��������� ������� ��������
		if(isPressed2 == true)
		{ // ������ ���� ������, ������ �� ������
			mass[button] = false;
			return 3; // 3 ���� ���� ��������
		}
		else
		{ // ������ ���� �� ������, ������ ������
			mass[button] = true;
			return 1; // 1 ���� ���� ������
		}
	}
	else
	{ // ��������� �� ��������
		if(isPressed2 == true) // ���� ������ ���� ������
			return 2; // 2 ���� ������ ������
		else
		  return 0; // 0 ���� ������ �� ������
	}
}

void Mouse::setDesign(sf::String filename)
{
	cursor->setDesign(filename);
}

void Mouse::setCursor(int number)
{
	cursor->setCursor(number);
}

void Mouse::draw()
{
	cursor->draw(window);
}

void Mouse::setMouseCursorVisible(bool boolean)
{
	window->setMouseCursorVisible(boolean);
	cursor->isVisible = !boolean;
}