//Desigh.h
//v2.0 07.01.2017
#ifndef DSL_DESIGN_H
#define DSL_DESIGN_H

#include "stdafx.h"

class Design
{
protected:
	sf::String filename; // ��� ����� �������
	sf::Image image;     // �������� ������ �������
	std::vector<sf::Image> vecImage; // ���������� ��������

	// ����� �������, ������� ����� ��� getImage(int numCell, int factorX, int factorY)
	//sf::Image getNewImage1(int numCell, int factorX, int factorY);
	//sf::Image getNewImage2(int numCell, int factorX, int factorY);
	//sf::Image getNewImageX(int numCell, int factor);
	//sf::Image getNewImageY(int numCell, int factor);
public:
	Design(void);
	~Design(void);

	void reserve(int num);

	bool loadImage(sf::String filename);

	// �������� ������� ������������� ������ � ����� ��������
  bool designToImage(sf::Image* image1, sf::Image* image2, int numCellWidth, int numCellHeight, int sideImage);
	bool designToImage(sf::Image* image1, sf::Image* image2, int numCellWidth, int numCellHeight, int sizeX, int sizeY);
	
	// ���������� �������� �������
	sf::Image getImage();
	
	// ���������� ���������� �������� ������� ��� �������
	sf::Image getImage(int numCell);
	
	// ���������� ���������� �������� ������� ��� ������� ����������� �� � � factorX ���, �� Y � factorY ���
  //sf::Image getImage(int numCell, int factorX, int factorY);
	
	sf::String getFilename();
	int size();
};

struct DesignWindow : public Design
{
	DesignWindow()
	{
		reserve(9);
	}
	DesignWindow(sf::String filename)
	{
		reserve(9);
		load(filename);
	}
	bool load(sf::String filename)
	{
		if(loadImage(filename))
		{
			// ������������ �������� � �������
			// 0 4 1
			// 5 8 6
			// 2 7 3
			// ����� image � ��������� �������� vecImage[0-8]
					designToImage(&image, &vecImage[0], 0, 0, 32);
			if(!designToImage(&image, &vecImage[1], 2, 0, 32)) { designToImage(&image, &vecImage[1], 0, 0, 32); vecImage[1].flipHorizontally(); }
					designToImage(&image, &vecImage[2], 0, 2, 32);
			if(!designToImage(&image, &vecImage[3], 2, 2, 32)) { designToImage(&image, &vecImage[3], 0, 2, 32); vecImage[3].flipHorizontally(); }
					designToImage(&image, &vecImage[4], 1, 0, 32);
					designToImage(&image, &vecImage[5], 0, 1, 32);
			if(!designToImage(&image, &vecImage[6], 2, 1, 32)) { designToImage(&image, &vecImage[6], 0, 1, 32); vecImage[6].flipHorizontally(); }
					designToImage(&image, &vecImage[7], 1, 2, 32);
					designToImage(&image, &vecImage[8], 1, 1, 32);

			return true;
		}
		return false;
	}
};

struct DesignCursor : public Design
{
	DesignCursor()
	{
		reserve(7);
	}
	DesignCursor(sf::String filename)
	{
		reserve(7);
		load(filename);
	}
	bool load(sf::String filename)
	{
		if(loadImage(filename))
		{
			designToImage(&image, &vecImage[0], 0, 0, 32); // ������
			designToImage(&image, &vecImage[1], 1, 0, 32); // �����������
			designToImage(&image, &vecImage[2], 2, 0, 32); // ������������
			designToImage(&image, &vecImage[3], 3, 0, 32); // ��������������
			designToImage(&image, &vecImage[4], 4, 0, 32); // ����-���� <-> �����-���
			designToImage(&image, &vecImage[5], 5, 0, 32); // ����-���  <-> �����-����
			designToImage(&image, &vecImage[6], 6, 0, 32); // ������

			return true;
		}
		return false;
	}
};

struct DesignButton : public Design
{
	DesignButton() { reserve(3); }
	DesignButton(sf::String filename, int num, bool flag)
	{
		reserve(num);
		load(filename, num, flag);
	}
	bool load(sf::String filename, int num, bool flag)
	{
		int sizeX, sizeY; 
		if(loadImage(filename))
		{
			sf::Vector2u size = image.getSize();
			if(flag)
			{
				sizeX = size.x / num;
				sizeY = size.y;
				for(int i=0; i<num; i++)
					designToImage(&image, &vecImage[i], i, 0, sizeX, sizeY);
			}
			else
			{
				sizeX = size.x;
				sizeY = size.y / num;

				for(int i=0; i<num; i++)
					designToImage(&image, &vecImage[i], 0, i, sizeX, sizeY);
			}
			return true;
		}
		return false;
	}
};

struct DesignTextBox : Design
{
	DesignTextBox() { reserve(3); }
	DesignTextBox(sf::String filename) { load(filename); }
	bool load(sf::String filename)
	{
		reserve(3);
		if(loadImage(filename))
		{
			// 0 2 1
			designToImage(&image, &vecImage[0], 0, 0, 32);
			designToImage(&image, &vecImage[1], 1, 0, 32);
			designToImage(&image, &vecImage[2], 2, 0, 32);
			//vecImage[2].flipHorizontally();

			return true;
		}
		return false;
	}
};

struct DesignWinChat : Design
{
	DesignWinChat() { reserve(10); }
	DesignWinChat(sf::String filename) { load(filename); }
	bool load(sf::String filename)
	{
		reserve(10);
		if(loadImage(filename))
		{
			// ������������ �������� � �������
			//  0  4  1
			//  5  8  6
			//  2  7  3
			//  - 10  -
			// ����� image � ��������� �������� vecImage[0-8]
			designToImage(&image, &vecImage[0], 0, 0, 32);
			designToImage(&image, &vecImage[1], 0, 0, 32); vecImage[1].flipHorizontally();
			designToImage(&image, &vecImage[2], 0, 2, 32);
			designToImage(&image, &vecImage[3], 0, 2, 32); vecImage[3].flipHorizontally();
			designToImage(&image, &vecImage[4], 1, 0, 32);
			designToImage(&image, &vecImage[5], 0, 1, 32);
			designToImage(&image, &vecImage[6], 0, 1, 32); vecImage[6].flipHorizontally();
			designToImage(&image, &vecImage[7], 1, 2, 32);
			designToImage(&image, &vecImage[8], 1, 1, 32);

			return true;
		}
		return false;
	}
};

#endif DSL_DESIGN_H
