//eventHandlerWindows.cpp
//v2.0 14.01.2017

#include "eventHandlerWindows.h"

// �����������
/*
EventHandlerWindows::EventHandlerWindows(void) { data(); }

// ����������
EventHandlerWindows::~EventHandlerWindows(void)
{
	// ����������� ������
	vecWindow.~vector();
	vecWindow2.~vector();
}
*/
// ����� ��������� ����������
void EventHandlerWindows::data(void)
{
	mouse = &(Mouse::Instance());
	winSelect = nullptr;
	winCur = nullptr;
	renderWindow = nullptr;
	design = new designWindows();
	design->vecWin = &vecWindow;
	flag0 = false;
	flag1 = false;
	flag2 = false;
	flag3 = false;
	flagButton = false;
	numWinActiv = 0;
	modeDrag  = false;
	modeDrag2 = false;
	////////////////////////////
	vecWindow.reserve(0);
	vecWindow2.reserve(0);
	////////////////////////////
}

// ��������� ���� � ���������� ����
void EventHandlerWindows::add(Window* window)
{
	window->setDesign(design->get());
	window->renderWindow = renderWindow;
	vecWindow.push_back(window);
	vecWindow2.push_back(window);
	numWinActiv++;
}

// ���������� ����������
// fasle - ����� �� ����������
bool EventHandlerWindows::eventKey(int type, int code)
{
	if(winSelect == nullptr)
		return false;
	else
		return winSelect->eventKey(type, code);
}
bool EventHandlerWindows::eventMouse(int type, int code)
{
	x = mouse->x;
	y = mouse->y;
	// ���� ��� �������� ����, ������ �� ���� ������
	if(numWinActiv == 0) return false;
	// ���� ������� ���� �� �����, ������ �� ���� ������
	if(code != sf::Mouse::Left) return false;

	switch(type)
	{
	case -1:
		if(flagx == false)
			behaviorCase0(); // ��� �� ������
		else
			behaviorCase2(); // ��� ��� ������ 
		break;
	// ��� ����������
	case 1:
		behaviorCase1(); 
		flagx = true;  
		break;
	// ��� �����������
	case 3: 
		behaviorCase3(); 
		flagx = false; 
		break; 
	}
	if(winSelect != nullptr)
		winSelect->eventMouse(type, code);
	
	return false;
}
void EventHandlerWindows::behavior(float time)
{
	if(winCur != nullptr)
		winCur->behavior(time);
}
// ���������� ����
void EventHandlerWindows::sort1(int num)
{
	Window* window = vecWindow[num];

	if(vecWindow.size() > 1)
	{
		for(int i=num; i>0; i--)
		{
			vecWindow[i] = vecWindow[i-1];
		}
		vecWindow[0] = window;

		vecWindow[0]->isTarget = true;
		vecWindow[1]->isTarget = false;
	}
}

// ��������� �� �������� ����
void EventHandlerWindows::draw(sf::RenderWindow* window)
{
	// ���� ���������� �������� ���� �� 0, ���� ������������
	if(numWinActiv != 0)
		for(int i=vecWindow.size()-1; i>=0; i--)
			vecWindow[i]->draw(window);
}

// ��������� � ������� ����������� ����
void EventHandlerWindows::draw()
{
	// ���� ���������� �������� ���� �� 0, ���� ������������
	if(renderWindow != nullptr && numWinActiv != 0)
		for(int i=vecWindow.size()-1; i>=0; i--)
			vecWindow[i]->draw(renderWindow);
}

// ���������� ��������� ���� ��� ������� num � ��������� flag
bool EventHandlerWindows::setVisibleWin(int num, bool flag)
{
	// ���� ���������� ���� ������ ������ ����
	if(vecWindow.size() > num)
	{
		// flag1 - ���������� ��������� ����
		bool flag1 = vecWindow[num]->isVisible;
		// ���� ���� ���� �������
		if(flag1 == true) 
			if(flag == false) numWinActiv--; else return false;
		else 
			if(flag == true)  numWinActiv++; else return false;
		vecWindow[num]->isVisible = flag;
	  return true;
	}
	return false;
}

void EventHandlerWindows::switchVisibleWin(int num)
{
	if(vecWindow.size() <= num) return;

	vecWindow2[num]->switchVisible();

	// ��� ����������� ��������� �������������� ����� ����������� ������ ���� ���������
	for(int i=0; i<vecWindow.size(); i++)
		if(vecWindow[i] == vecWindow2[num])
		{
			sort1(i);
			break;
		}
}

void EventHandlerWindows::behaviorCase0()
{
	resetFlag0();
	searchWindow();
	behaviorButton();
	behaviorCursor();
	checkModeDrag0();
}
void EventHandlerWindows::behaviorCase1()
{
	for(std::vector<Window*>::iterator it = vecWindow.begin(); it != vecWindow.end(); it++)
		if(checkWindow(it))
			break;
}
void EventHandlerWindows::behaviorCase2()
{
	if(!behaviorButton2())
		controlWindow();
	checkModeDrag2();
}
void EventHandlerWindows::behaviorCase3()
{
	behaviorButton3();
	behaviorCheckBox();
	setToZer0();
}

//behaviorCase0 : ����������

// ���������� ���� ��������� �� ����
void EventHandlerWindows::resetFlag0()
{
	// ���� �� ����� ���� ��������� �� ����
	if(flag0 == true){
		// � ������ ������ �� � ����
		if(winCur->check1(x, y) == false){
			winCur->resetSelect();
			flag0 = false;
		}
	}
}
void EventHandlerWindows::searchWindow()
{
	// <�����������> �� 13.04.2017 10:55 (����� �������)
	// ����� ������ ����� ����, ����� �� ����� �� �������?
	// </�����������>
	if(flag0 == false){
		// ���� ����� ������ ���� �� ������� �������� �����
		for(std::vector<Window*>::iterator it = vecWindow.begin(); it != vecWindow.end(); it++){
			if((*it)->check1(x, y)){
				winCur = *it; // ���������� ������ �� ����
				flag0 = true;
				return;
			}
		}
		winCur = nullptr;
	}
}
void EventHandlerWindows::behaviorButton()
{
	// ���� ���� ����
	if(winCur != nullptr)
		winCur->behaviorButton(0);
}
void EventHandlerWindows::behaviorCursor()
{
	// ���� ���� ����
	if(winCur != nullptr){
		numRote = winCur->check3(mouse->x, mouse->y);
		winCur->behaviorCursor();
	} else 
		mouse->setCursor(0);
}
void EventHandlerWindows::checkModeDrag0()
{
	// ����� ��������������
	if(modeDrag)
		for(int i=1; i<vecWindow.size(); i++)
			if(!vecWindow[0]->check1(x, y) && vecWindow[i]->check1(x, y))
			{
				sort1(i);
				break;
			}
}

//behaviorCase1 : ����������
// MouseButtonPressed
bool EventHandlerWindows::checkWindow(std::vector<Window*>::iterator it)
{
	if((*it)->isVisible && (*it)->check1(x, y) == true && winCur != nullptr)
	{
		winSelect = winCur;
		flag1 = true;
		// ��������� ���� ������ �� �������� ���� ��, ������� ���� ��������
		sort1(it - vecWindow.begin());
		
		winCur->behaviorButton(1);
		if(winCur->curItem != nullptr && winCur->curItem->type == WINBUTTON){
			flagButton = true; // ��������� �� ������ -> ������� �� �����
			winCur->curItem->setTile(2);
		}

		//=====================
		if(numRote != -1)              flag3 = true; // ���� ���� ������������ -> ������� � ������������� ����
		if(vecWindow[0]->check2(x, y)) flag2 = true; // ������� � ��������������� ����
		if((flag2 || flag3 || flagButton) && modeDrag)
			switchModeDrag();
		// ���������� ���������� ���� � ����
		winCur->save(mouse->pos, numRote);
		return true; // �������������� ������� �� ������� ����, �.�. �� ����� ���� �� ������� ��������.
	}
	return false;
}

//behaviorCase2 : ����������
bool EventHandlerWindows::behaviorButton2()
{
	if(flagButton)
	{
		// ���� �� 14:27 24.12.2016
		if(modeDrag)
		{
			modeDrag = false;
			modeDrag2 = true;
		}

		winCur->behaviorButton(2);

		return true;
	}
	return false;
}
void EventHandlerWindows::controlWindow()
{
	// ������������ ����
	if(flag3) 
		vecWindow[0]->changeSize();
	// ����������� ����
	else if(flag2)
		vecWindow[0]->changePosition();
}
void EventHandlerWindows::checkModeDrag2()
{
	if(modeDrag)
		for(int i=1; i<vecWindow.size(); i++)
			if(!vecWindow[0]->check1(x, y) && vecWindow[i]->check1(x, y))
			{
				sort1(i);
				break;
			}
}

//behaviorCase3 : ����������
// MouseButtonReleased
void EventHandlerWindows::behaviorButton3()
{
	// ���� ���� ������ ������ � ���� ����
	  // ������������ ��������� ������ (1 - �������� ����)
	    // ���������� ���� ����������� ���� ����� 1
	if(flagButton && winSelect != nullptr)
		if(winSelect->behaviorButton(3) == 1)
			winSelect = nullptr;
}
void EventHandlerWindows::behaviorCheckBox()
{
	// ��������� winCheckBox
	if(winCur != nullptr)
		for(int i=0; i<winCur->vecCheckBox.size(); i++)
		{
			if(winCur->vecCheckBox[i]->check1(x, y))
			{
				winCur->vecCheckBox[i]->switchFlag();
				switch(i)
				{
				case 0: winCur->switchConsiderBorder(); break;
				case 1: winCur->switchMove();           break;
				case 2: winCur->switchChangeSize();     break;
				}
			}
		}
}
void EventHandlerWindows::setToZer0()
{
	flag1 = false;
	flag2 = false;
	flag3 = false;
	flagButton = false;
	switchModeDrag();
	numRote = 0;
}

void EventHandlerWindows::switchModeDrag()
{
	if(modeDrag){
		modeDrag = false;
		modeDrag2 = true;
	} else if(modeDrag2){
		modeDrag = true;
		modeDrag2 = false;
	}
}