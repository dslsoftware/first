#pragma once
#include "stdafx.h"
#include "myMath.h"
#include "Tilemovi.h"
#include "Font.h"
//#include "BaseChar.h"


// ����� ��������, ������ � �������, ��� ���������� ��������� ����� (������ ��������)
struct sAttack
{
	Image line1; // ����� ������
	Image line2; // ����� �����
	sf::Vector2f t1; // x - t1cur, y - t1max
	sf::Vector2f t2; // x - t2cur, y - t2max
  bool flag1; // true - ����� �����
	void data()
	{
		t1.x = 0;
		t1.y = 2; // ����� ����� �������
		t2.x = 0;
		t2.y = 1; // ����� ��������� �����
		
		line1.loadFromFile("test/line1.png");
	  line1.setScale(0,1);
	  line1.setOriginNum(4);

		line2.loadFromFile("test/line2.png");
	  line2.setScale(0,1);
	  line2.setOriginNum(4);

		flag1 = false; // ����� �� �����
	}
	sAttack() { data(); }
	// wave - �����
	// ���������� true ���� wave �����
	bool wave(float time)
	{
		if(flag1 == false) 
		{
			t1.x += time;
			if(t1.x >= t1.y) { 
				// ����� �������
				t2.x = t1.x - t1.y; // ��������
				flag1 = true; // ���������� �� ��������� �����
				line1.setScale(100, 1);
				return true;
			}
			line1.setScale(100 * t1.x / t1.y, 1);
		}
		return false;
	}
	// ��������� �����
	bool attack(float time)
	{
		if(flag1 == true) {
			t2.x += time;
			if(t2.x >= t2.y) {
				// ����
				t1.x = t2.x - t2.y; // ��������
				flag1 = false;
				line2.setScale(0, 1);
				line1.setScale(0, 1);
				return true;
			}
			line2.setScale(100 * t2.x / t2.y, 1);
		} 
		return false;
	}
	void cancelAttack()
	{
		t2.x = 0;
		line2.setScale(0, 1);
	}
	void setPosition(float x, float y)
	{
		line1.setPosition(x, y);
		line2.setPosition(x, y);
	}
	void move(float x, float y)
	{
		line1.move(x, y);
		line2.move(x, y);
	}
	void draw(sf::RenderWindow* window)
	{
		if(flag1 == true) {
		  line1.draw(window);
		  line2.draw(window);
		}
		else
			line1.draw(window);
	}
};
// ����� �������, ������ � ������� ��� ����������� ���������� �����
struct textChange
{
	std::vector<float> vecTime;
	std::vector<float> vecAlpha;
	std::vector<sf::Color> vecColor;
	std::vector<sf::Text> vecText;
	
	int charSize;
	int timeMax;
	float dalpha;
	sf::Vector2f speed;
	sf::Vector2f offset; // �������� �� ���������
	void data(void)
	{
		charSize = 10;
		speed.x = 0;
		speed.y = -50;
		timeMax = 2;
		dalpha = 255/timeMax;
	}
	textChange() { data(); }
	void add(sf::Vector2f pos, int dx, int dy, sf::String str, sf::Color color)
	{
		sf::Text text;
		text.setFont(*Font::Instance());
		text.setCharacterSize(charSize);
	  text.setColor(color);
		text.setString(str);
		text.setOrigin(text.getLocalBounds().width/2,0);
		text.setPosition(pos.x + offset.x + dx, pos.y + offset.y + dy);

		vecText.push_back(text);
		vecColor.push_back(color);
		vecAlpha.push_back(color.a);
		vecTime.push_back(0);
	}
	void setOffset(sf::Vector2f pos) { offset = pos; }
	void behavior(float time)
	{
		for(int i=0; i<vecText.size(); i++)
		{
			vecTime[i] += time;
			if(vecTime[i] < timeMax)
			{
				vecAlpha[i] -= dalpha*time;
				vecColor[i].a = vecAlpha[i];
				vecText[i].setColor(vecColor[i]);
				vecText[i].move(speed*time);
			}
			else
			{
				vecText.erase(vecText.begin());
				vecColor.erase(vecColor.begin());
				vecTime.erase(vecTime.begin());
				vecAlpha.erase(vecAlpha.begin());
			}
		}
	}
	void draw(sf::RenderWindow* window)
	{
		for(int i=0; i<vecText.size(); i++)
			window->draw(vecText[i]);
	}
};
struct textNickname
{
	sf::Vector2f offset;
	sf::Text text;
	textNickname() 
	{ 
		text.setFont(*Font::Instance());
		text.setColor(sf::Color::Black);
		text.setCharacterSize(16);
	}
	void setString(std::string str)
	{
		text.setString(str);
	}
	void setOffset(int dx, int dy)
	{
		offset = sf::Vector2f(dx, dy);
	}
	void setPosition(int x, int y)
	{
		text.setPosition(offset.x+x, offset.y+y);
	}
	void move(float offsetX, float offsetY)
	{
		text.move(offsetX, offsetY);
	}
	void draw(sf::RenderWindow* window) 
	{ 
		window->draw(text); 
	}
};
// Round - �������
// vecPoint[0] - ��������� �����
// vecPoint[1] - ������� ��������������
// vecPoint[2] - �������� �����
struct Round
{
	float angle; // ���� ����� ��������� � �������� �������
	float distanceCur; // ���������� �� �������� �������������� �� ��� �����
	float distanceMax; // ���������� �� ������ �������� �� ��� �����
	std::vector<sf::Vector2f> vecPoint;

	void data()
	{
		vecPoint.reserve(3);
		for(int i=0; i<3; i++)
		{
			sf::Vector2f point;
			point.x = 0;
			point.y = 0;
			vecPoint.push_back(point);
		}
	}
	// ������� ��������� ��������� �������
	void update(float x, float y)
	{
		// ��������� ���������� �������� ���������
		vecPoint[1].x = x;
		vecPoint[1].y = y;
		// ��������� ��������� �� �������� ��������� �� ����� ����������
		distanceCur = distance(vecPoint[1].x, vecPoint[1].y, vecPoint[2].x,  vecPoint[2].y);
	}
	// ������� ����� ����� ������ � ���� ���� ���������
	void newPoint(float x1, float y1, float x2, float y2)
	{
		// ������
	  vecPoint[0].x = x1;
		vecPoint[0].y = y1;
		// ��� ������
		vecPoint[1].x = x1;
		vecPoint[1].y = y1;
		// ����
		vecPoint[2].x = x2;
		vecPoint[2].y = y2;
		// ���������� ����
		angle = d_angle(x2-x1, y2-y1);
		// ���������� ���������� �� ������ �� �����
		distanceMax = distance(x1, y1, x2, y2);
		// ������� ���������� = �������������, �.�. ������ ��� ������� ����� �����
		distanceCur = distanceMax;
	}
};

class Object1
{
public:
	//Object1* target;
	//character* CHAR;
	//void addChar(character* char_);
	//characteristics CHAR;
	//std::vector<_Cell*> vecCells;
	sAttack lineAttack;
	textChange text1;
	textNickname nickname;

	Round round;
	int status; // ������ ������� // 0 - ������ �� ���� ������
	                              // 1 - ���� ���������
	sf::Vector2f vecSpeed; // �������� �� ��� � � Y
	Tilemovi image; // ����� �������
	Image shadow;
  Image lineHP;
	Image lineMP;
public:
	void data();

	Object1(void);
	~Object1(void);
	void set_nickname(std::string nickname);
	bool loadFromFile(std::string filename);
	void draw(sf::RenderWindow* window);

	void behavior(float time);
	void behavior_movement(float time);
	void behavior_follow_target(float time);
	void behavior_attack(float time);
	void behavior_stop();

	void setPosition(float x, float y);
	void move(float x, float y);
	void move2(float speed, float angle);
	sf::Vector2f getPosition();

	void update_hp(bool flag, float value);
	void update_mp(bool flag, float value);
	void newTargetMove(int settatus, float x, float y);

//////////////////����� ���������� ������� ��� ������///////////////////////////////////



////////////////////////////////////////////////////////////////////////////

	//virtual void setCharacter(std::vector<BaseChar*> &vec);
	//void setChar(character* CHAR);
	//void setRoundCell(std::vector<_Cell*> vec);

	bool nextPos();
};

