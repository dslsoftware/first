#pragma once
#include "stdafx.h"
#include "myMath.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "Font.h"

class Text : public sf::Text
{
public:
	// ���������� ������ �������
	int findCharacterSizeX(int index) const
	{
		if(index < 0) return 0;
		return findCharacterPos(index+1).x - findCharacterPos(index).x;
	}
	// ���������� ������ ������ �������
	sf::Vector2i findCharacterSizeDX(int indexBegin, int indexEnd)
	{
		int x = 100;
		if(indexBegin < indexEnd) x = findCharacterPos(indexBegin).x;
		else                      x = findCharacterPos(indexEnd).x;

		return sf::Vector2i(x, myMod(findCharacterPos(indexEnd).x - findCharacterPos(indexBegin).x));
	}
	void insert(int position, sf::String str)
	{
		if(position < 0) position = 0;
		sf::String strx = getString();
		strx.insert(position, str);
		setString(strx);
	}
	void erase(int index)
	{
		if(index < 0) return;
		sf::String strx = getString();
		strx.erase(index, 1);
		setString(strx);
	}
	void erase(int index, int num)
	{
		if(index < 0) return;
		sf::String strx = getString();
		strx.erase(index, num);
		setString(strx);
	}
	virtual void operator=(sf::String str)
	{
		setString(str);
	}
	virtual void operator+=(sf::String str)
	{
		sf::String strx = getString();
		strx += str;
		setString(strx);
	}
	size_t getSize() const { return getString().getSize(); }
};

struct imgx
{
	sf::Image   image;
	sf::Texture texture;
	sf::Sprite  sprite;
	imgx(int w, int h, sf::Color color)
	{
		image.create(w, h, color);
		texture.loadFromImage(image);
		sprite.setTexture(texture);
	}
	imgx(sf::Image img)
	{
		image = img;
		texture.loadFromImage(image);
		sprite.setTexture(texture);
	}
	void move(int dx, int dy) { sprite.move(dx, dy); }
	void setPosition(int x, int y) { sprite.setPosition(x, y); }
	void draw(sf::RenderWindow* window) { window->draw(sprite);	}
};

class textBox
{
private:
	bool focus;  //
	bool rus;
	bool fmouse; // ������ �� ����� ������ ������� ����
	bool blink;  // ������������� ��������
	bool gblink; // global blink
	vec2f mousepos;
	sf::String str1;
	sf::String str2;
	sf::String str3;
	sf::String str4;
	Timer* timer; // ������ Pipe
  //imgx* image; 
	int index2; // �������� : �� ����
	imgx* pipe; // ���� ���������
	int index; // ������� ������
	Mouse* mouse;
public:
	imgx* x1;
	imgx* x2;
	imgx* x3;
	Text text;
	int maxsize; // ������������ ������
public:
	textBox(int w, int h)
	{
		mouse = &Mouse::Instance();
		maxsize = 16;
		fmouse = false;
		focus = false;
		blink = false;
		str1 = "abcdefghijklmnopqrstuvwxyz0123456789          [];,.'/\\`=- ";
		str2 = "��������������������������0123456789          ������.\\�=- ";
		str3 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ)!@#$%^&*(          {}:<>\"?|~+_ ";
		str4 = "��������������������������)!\"�;%:?*(          ������,/�+_ ";
		rus = false;

		index2 = -1;

		index = 0;
		//image = new imgx(w, h, sf::Color::Black);
		pipe  = new imgx(1, h, sf::Color(0, 255, 255));
		text.setFont(*Font::Instance());
		text.setCharacterSize(16);
		text.setColor(sf::Color::Black);
		timer = new Timer(0.5);
		timer->start();
	}
	~textBox(void) { }
	bool eventKey(int type, int code)
	{
		if(type == 1){
			// pressed
			if(focus)
			{
				bool isShift = (keyboard.getEventButton(sf::Keyboard::LShift) == 2) 
					          || (keyboard.getEventButton(sf::Keyboard::RShift) == 2);
				bool isAlt   = (keyboard.getEventButton(sf::Keyboard::LAlt) == 2) 
					          || (keyboard.getEventButton(sf::Keyboard::RAlt) == 2);
				// ���� ����� ����
				if(isShift) {
					if(0 <= code && code <= 35 || 46 <= code && code <= 57){
						erase(-1);
						if(rus == true) insert(str4[code]);
											 else insert(str3[code]);
					}
				} else {
					if(0 <= code && code <= 35 || 46 <= code && code <= 57){
						erase(-1);
						if(rus == true) insert(str2[code]);
											 else insert(str1[code]);
					}
				}
				//backspace
				if(code == 59) 
					erase(0);
				//del
				if(code == 66) 
					erase(1);
				//<-
				if(code == 71)           
				{
					if(isShift)
						selection(-1);
					else if(reset(1) == false)
						prev();
				}
				//->
				if(code == 72)
				{
					if(isShift) 
						selection(1);
					else if(reset(2) == false)
							next();
				}
				// ����� �����
				if(isShift && (code == 39 || code == 43)
					|| isAlt && (code == 38 || code == 42))
					rus = !rus;
				//...
			}
		}//if(type == 1)
		return true;
	}
	void behavior(float time)
	{
		if(pipe->sprite.getScale().x == 1) {
			if(timer->update(time) == false) { 
				blink = !blink;
				timer->start();
			}
		}
	}
	void eventMouse(int type, int code)
	{
		if(focus && fmouse)
			selection(0);

		if(code == sf::Mouse::Left){
			if(type == 3){
				fmouse = false;
				if(index == index2) 
				reset(0);
			} else if(type == 1){
				reset(0);
				pipe->sprite.setColor(sf::Color::Cyan);
				fmouse = true;
				mousepos = vec2f(mouse->pos.x, mouse->pos.y);

				if(check1(mousepos))
					focus = true;
				else
					focus = false;

				if(focus == true) {
					for(int i=0; i<text.getSize(); i++) {
						vec2f vec = text.findCharacterPos(i);
						int w = text.findCharacterSizeX(i);
						if(vec.x + w > mousepos.x) {
							updatePipe(i);
							break;
						}
					}
				} 
			}
		}
	}

	void draw(sf::RenderWindow* window)
	{
		x1->draw(window);
		x2->draw(window);
		x3->draw(window);
		//image->draw(window);
		if(pipe->sprite.getScale().x != 1 || focus && blink) 
			pipe->draw(window);
		window->draw(text);
	}
	bool check1(sf::Vector2f vec)
	{
		if(x1->sprite.getPosition().x <= vec.x && vec.x <= x3->sprite.getPosition().x + x3->image.getSize().x
		&& x1->sprite.getPosition().y <= vec.y && vec.y <= x1->sprite.getPosition().y + x1->image.getSize().y)
			return true;
		return false;
	}
	
	void move(int dx, int dy) 
	{ 
		//image->move(dx, dy);
		x1->move(dx, dy);
		x2->move(dx, dy);
		x3->move(dx, dy);
		pipe->move(dx, dy);
		text.move(dx, dy);
	}
	void setPosition(int x, int y) 
	{ 
		//image->setPosition(x, y);
		x1->setPosition(x, y);
		x2->setPosition(x1->sprite.getPosition().x + x1->image.getSize().x, y);
		x3->setPosition(x2->sprite.getPosition().x + x2->sprite.getScale().x, y);
		pipe->setPosition(x, y);
		text.setPosition(x+5, y);
	}

	void selection(int code)
	{
		pipe->sprite.setColor(sf::Color::Cyan);
		if(code == 0) {
			//================
			int dx = mouse->x - mousepos.x;
			bool flag1 = false;
			for(int i=0; i<text.getSize(); i++) {
				vec2f vec = text.findCharacterPos(i);
				int w = text.findCharacterSizeX(i);
				if(vec.x + w > mouse->x) {
					index2 = i;
					flag1 = true;
					break;
				}
			}
			if(!flag1)
				updatePipe(text.getString().getSize());
			if(dx > 0)
				index2++;
			//================
		} else if(code == -1) {
			prev();
			if(index2 == -1)
				index2 = index+1;
		} else if(code == 1) {
			next();
			if(index2 == -1)
				index2 = index-1;
		}
		sf::Vector2i vec = text.findCharacterSizeDX(index, index2);
		pipe->setPosition(vec.x, text.getPosition().y);
		pipe->sprite.setScale(vec.y, 1);
	}

	void next()
	{
		if(index < text.getString().getSize())
		{
			pipe->move(text.findCharacterSizeX(index),0);
			index++;
		}
	}
	void prev()
	{
		if(index != 0) 
		{
			index--;
			pipe->move(-text.findCharacterSizeX(index),0);
		}
	}
	void insert(sf::String str)
	{
		if(maxsize == 0 || maxsize > text.getString().getSize())
		{
			text.insert(index, str);
			next();
		}
	}
	void erase(int n) 
	{ 
		if(index2 != -1)
			erase(myMin(index, index2), myMod(index2-index));

		else if(n == 0)
		{
			if(index != 0)
			{
				prev();
				text.erase(index);
			}
		} else if(n > 0)
			text.erase(index);
	}
	void erase(int begin, int num)
	{
		if(index != index2 && index != -1 && index2 != -1) 
		{
			text.erase(begin, num);
			index = myMin(index, index2);
			updatePipe(index);
			reset(1);
		}
	}
	bool reset(int code)
	{
		if(index2 >= 0)
		{
			if(code == 1) {
				index = myMin(index, index2);
			} else if(code == 2) {
				index = myMax(index, index2);
			}
			blink = true;
			updatePipe(index);
			pipe->sprite.setScale(1,1);
			pipe->sprite.setColor(sf::Color::Black);
			index2 = -1;
			return true;
		}
		return false; // ������ �� ����
	}
	void updatePipe(int newIndex)
	{
		index = newIndex;
		pipe->setPosition(text.findCharacterPos(index).x, text.getPosition().y);
	}
};

class Label : public Text
{
private:
	void data()
	{
		setFont(*Font::Instance());
		setCharacterSize(16);
		setColor(sf::Color::Black);
	}
public:
	Label() { data(); }
	void draw(sf::RenderWindow* window) 
	{
		window->draw(*this);
	}
};
