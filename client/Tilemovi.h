//Tilemove.h
//v2.0 07.01.2017

#ifndef DSL_TILEMOVI_H
#define DSL_TILEMOVI_H

#include "stdafx.h"
#include "tileset.h"

class Tilemovi :
	public Tileset
{
private:
	int numIterationCur; // ����� �������
	int numIterationMax; // ������������ ���������� ���������� �����
	float timeFrameCur; // ����� ��� ����������
  float timeFrameMax; // ����� ��� ����� �����
  
	bool function1(float time);
public:
	Tilemovi(void);
	Tilemovi(sf::String filename);
	~Tilemovi(void);
	void data();

	bool shift_frame(float time);
  void data_time_frame(float time_shift_frame);
	void data_number_iteration(int numIteration);
	void play();
};

#endif DSL_TILEMOVI_H
