//Window.cpp
//v2.0 07.01.2017
#include "Window.h"

void Window::data()
{
	//12.04.2017 13:42
	curItem = nullptr;
	itemSelect = nullptr;

	mouse = &Mouse::Instance();

	border = vec4i(8, 31, 8, 8); // ��� ������� win8.1
	setSizeMin(100, 100);

	text = nullptr;
	renderWindow = nullptr;

	isMove = true;
	isConsiderBorder = false;
	isChangeSize = true;
	isVisible = true;
	isTarget = false;
	flagMoves = false;

	vecObj.reserve(0);
	vecImage.reserve(0);
	vecButton.reserve(0);
	vecSlot.reserve(0);
	vecCheckBox.reserve(0);
	vecSprite.reserve(0);
	vecTextBox.reserve(0);
	vecLabel.reserve(0);

	addItem(WINBUTTON,   "images/Design/Button/butCloseWin8.1.png", vec2i(45, -30), 1, -4); // ������ ��������
	addItem(WINCHECKBOX, "images/Design/Button/check.png",          vec2i( 0, -30), 0, -4); // ���� �������
	addItem(WINCHECKBOX, "images/Design/Button/check.png",          vec2i(25, -30), 0, -4); // ���������� ����������
	addItem(WINCHECKBOX, "images/Design/Button/check.png",          vec2i(50, -30), 0, -4); // ��������� ��������� ��������

	wSave = new winSave(); 
}

Window::Window() { data(); }
Window::Window(int width, int height)
{
	data();
	setSize(width, height);
}
Window::Window(int width, int height, bool isMove, bool isConsiderBorder, bool isChangeSize)
{
	data();
	setSize(width, height);
	this->isMove = isMove;
	this->isConsiderBorder = isConsiderBorder;
	this->isChangeSize = isChangeSize;
}
Window::~Window(void) { }

bool Window::eventKey(int type, int code)
{
	// ���������� �� ����������
	if(itemSelect == nullptr)
		return false;
	//=========================
	if(itemSelect->type != WINTEXTBOX)
		return false;
	//=========================
	return itemSelect->eventKey(type, code);
}
void Window::eventMouse(int type, int code)
{
	if(itemSelect != nullptr)
		itemSelect->eventMouse(type, code);
}

void Window::resetSelect()
{
	if(curItem != nullptr)
		curItem->setTile(0);
}
int  Window::behaviorButton(int code)
{
	int numBut = checkButton(mouse->x, mouse->y);
	int numTextBox = checkTextBox(mouse->x, mouse->y);
	//=======================================
	// ��������� ��������� ����
	//=======================================
	if(code == 0){
		// ��������� �� Button
		if(numBut != -1){
			if(curItem != nullptr) 
				curItem->setTile(0);
			// ��������� ����� ������
			curItem = vecButton[numBut];
			curItem->setTile(1);
		} 
		// ��������� �� textBox
		else if(numTextBox != -1){
			if(curItem != nullptr)
				curItem->setTile(0);
			curItem = vecTextBox[numTextBox];
		} 
		// ��� ���������
		else {
			if(curItem != nullptr) {
				curItem->setTile(0);
				curItem = nullptr;
			} 
		}
	} 
	//=======================================
	// ������ ������� ���� (1 ���)
	//=======================================
	else if(code == 1){
		if(numTextBox != -1 || numBut != -1)
			itemSelect = curItem; 
		else
			itemSelect = nullptr;
	} 
	//=======================================
	// ���� ������
	//=======================================
	else if(code == 2){
		if(numBut != -1){
			if(vecButton[numBut] != curItem)
				vecButton[numBut]->setTile(1);
			else
				vecButton[numBut]->setTile(2);
		}
	} 
	//=======================================
	// ������ ���������� ������ ���� (1 ���)
	//=======================================
	else if(code == 3){
		if(numBut == 0 && !vecButton[0]->isLocked)	{
			isVisible = false;
			// ������ � �������� ���� EHW
			return 1;
		}
		else if(numBut > 0)
		{
			if(vecButton[numBut]->func != nullptr)
				vecButton[numBut]->beginFunc(vecTextBox[0]->getString(), vecTextBox[1]->getString());
			else if(vecButton[numBut]->func2 != nullptr)
				vecButton[numBut]->beginFunc2(vecTextBox[0]->getString());
		}
	}
	return 0;
}
void Window::behaviorCursor()
{
	// ��������� ������� �� ���� ����������
	// numRote - ����������� ���������� (-1 - ��� �����������)
	int numRote = check3(mouse->x, mouse->y);
	if(numRote != -1)
	{
		if(numRote == 0 || numRote == 3) mouse->setCursor(4);
		if(numRote == 1 || numRote == 2) mouse->setCursor(5);
		if(numRote == 4 || numRote == 7) mouse->setCursor(2);
		if(numRote == 5 || numRote == 6) mouse->setCursor(3);
	} else {
		// ��� ��������� �� ���� ��������������
		if(check2(mouse->x, mouse->y) && isMove) 
			mouse->setCursor(1);
		else
			mouse->setCursor(0);
		// ��� ��������� �� ������
		if(checkButton(mouse->x, mouse->y) != -1 || checkCheckBox(mouse->x, mouse->y) != -1) 
			mouse->setCursor(6);
	}
}

void Window::behavior(float time)
{
	if(itemSelect != nullptr)
		itemSelect->behavior(time);
}

void Window::setText(sf::String str)
{
	if(text!= nullptr)	
	{
		text->clear();
		text->add(str);
	} else {
		text = new mcText();
		text->add(str);
	}
}

void Window::draw(sf::RenderWindow *window)
{
	if(isVisible == true)
	{
		for(int i = vecSprite.size();    i > 0; i--) vecSprite[i-1]->draw(window);
		for(int i = vecObj.size();       i > 0; i--) vecObj[i-1]->draw(window);
		for(int i = vecSlot.size();      i > 0; i--) vecSlot[i-1]->draw(window);
		for(int i = vecButton.size();    i > 0; i--) vecButton[i-1]->draw(window);
		for(int i = vecCheckBox.size();  i > 0; i--) vecCheckBox[i-1]->draw(window);
		for(int i = vecTextBox.size();   i > 0; i--) vecTextBox[i-1]->draw(window);
		for(int i = vecLabel.size();     i > 0; i--) vecLabel[i-1]->draw(window);

		if(text != nullptr)
			text->draw(window);

		flagMoves = false; // �������� ����
	}
}
// �������������� � eventHandlerWindow
void Window::save(sf::Vector2i mousePos, int numRot)
{
	wSave->mousePos = mousePos;
	wSave->winPos = pos;
	wSave->winSize = size;
	switch(numRot)
	{
	case 0: wSave->winPos.x += wSave->winSize.x; 
					wSave->winPos.y += wSave->winSize.y; break;
	case 1: wSave->winPos.y += wSave->winSize.y; break;
	case 2: wSave->winPos.x += wSave->winSize.x; break;
	case 4: wSave->winPos.x += wSave->winSize.x;
					wSave->winPos.y += wSave->winSize.y; break;
	case 5: wSave->winPos.x += wSave->winSize.x; 
					wSave->winPos.y += wSave->winSize.y; break;
	}
	wSave->numRot = numRot;
}

void Window::setSize(int width, int height)
{
	if( width > 0 ) { if( width >= sizeMin.x) size.x =  width; else size.x = sizeMin.x; }
	if(height > 0 ) { if(height >= sizeMin.y) size.y = height; else size.y = sizeMin.y; }
	update();
}
//////////////////////////////////////////////
// ���������� ���������
//////////////////////////////////////////////
void Window::setDesign(DesignWindow* desWin)
{
	designWindow = desWin;
	std::vector<int> w; // ������ �����
	std::vector<int> h; // ������ �����

	for(int i=0; i<9; i++) { w.push_back(0); h.push_back(0); }

	for(int i=0; i<9; i++)
		addItem(WINSPRITE, designWindow->getImage(i), vec2i(0, 0), 0, 0);

	for(int i=0; i<9; i++)
	{
		w[i] = vecSprite[i]->size.x;
		h[i] = vecSprite[i]->size.y;
	}		
	// 2. ����� ������� ��������
	vecSprite[4]->setScale(size.x-w[0]-w[1], 1               );
	vecSprite[5]->setScale(               1, size.y-h[0]-h[2]);
	vecSprite[6]->setScale(               1, size.y-h[1]-h[3]);
	vecSprite[7]->setScale(size.x-w[2]-w[3], 1               );
	vecSprite[8]->setScale(size.x-w[5]-w[6], size.y-h[4]-h[7]);

	for(int i=0; i<9; i++)
	{
		w[i] = vecSprite[i]->size.x;
		h[i] = vecSprite[i]->size.y;
	}
	// 3. ����� �������� ��������
	vecSprite[0]->set(0, vec2i(          0, 0          ));
	vecSprite[1]->set(0, vec2i(size.x-w[1], 0          ));
	vecSprite[2]->set(0, vec2i(          0, size.y-h[2]));
	vecSprite[3]->set(0, vec2i(size.x-w[3], size.y-h[3]));
	vecSprite[4]->set(0, vec2i(       w[0], 0          ));
	vecSprite[5]->set(0, vec2i(          0, h[0]       ));
	vecSprite[6]->set(0, vec2i(size.x-w[6], h[1]       ));
	vecSprite[7]->set(0, vec2i(       w[2], size.y-h[7]));
	vecSprite[8]->set(0, vec2i(       w[0], h[0]       ));

	setPosition(pos.x, pos.y);

	border = vec4i(w[5], h[4], w[6], h[8]);

	update();
}
void Window::update()
{
	if(vecSprite.size() > 0)
	{
		std::vector<int> w; // ������ �����
		std::vector<int> h; // ������ �����
		for(int i=0; i<9; i++)
		{
			w.push_back(0);
			h.push_back(0);
		}

		for(int i=0; i<9; i++) 
		{ 
			w[i] = vecSprite[i]->size.x; 
			h[i] = vecSprite[i]->size.y;
		}
	
		vecSprite[4]->setScale(size.x-w[0]-w[1], 1               );
		vecSprite[5]->setScale(               1, size.y-h[0]-h[2]);
		vecSprite[6]->setScale(               1, size.y-h[1]-h[3]);
		vecSprite[7]->setScale(size.x-w[2]-w[3], 1               );
		vecSprite[8]->setScale(size.x-w[5]-w[6], size.y-h[4]-h[7]);

		for(int i=0; i<9; i++) { w[i] = vecSprite[i]->size.x; h[i] = vecSprite[i]->size.y; }

		vecSprite[1]->offset = vec2i(size.x-w[1], 0          );
		vecSprite[2]->offset = vec2i(          0, size.y-h[2]);
		vecSprite[3]->offset = vec2i(size.x-w[3], size.y-h[3]);
		vecSprite[6]->offset = vec2i(size.x-w[6], h[1]       );
		vecSprite[7]->offset = vec2i(       w[2], size.y-h[7]);

		setPosition(pos.x, pos.y);

		border = vec4i(w[5], h[4], w[6], h[8]);
	}
}

/////////////////////////////////////////
// ����������
/////////////////////////////////////////
void Window::setPosition(int x, int y)
{
	flagMoves = true;

	int xmax = 2000;
	int ymax = 2000;

	if(renderWindow != nullptr)
	{
	  xmax = renderWindow->getSize().x;
	  ymax = renderWindow->getSize().y;
	}

	if(!isConsiderBorder)
	{
		if(x < 0) x = 0;
		if(y < 0) y = 0;
		if(x > xmax - size.x) x = xmax - size.x;
		if(y > ymax - size.y) y = ymax - size.y;
	}
	pos = vec2i(x, y);
	
	for(int i=0; i<vecObj.size();       i++) vecObj[i]->update();
	for(int i=0; i<vecButton.size();    i++) vecButton[i]->update();
	for(int i=0; i<vecSlot.size();      i++) vecSlot[i]->update();
	for(int i=0; i<vecCheckBox.size();  i++) vecCheckBox[i]->update();
	for(int i=0; i<vecSprite.size();    i++) vecSprite[i]->update();
	for(int i=0; i<vecTextBox.size();   i++) vecTextBox[i]->update();
	for(int i=0; i<vecLabel.size();     i++) vecLabel[i]->update();

	if(text != nullptr)
		text->setPosition(pos.x + size.x/2 - text->getW(1)/2, pos.y+5);
}
// ��������� �������� ����
void Window::changeSize()
{
	// ���� ����� �������� ������� ����
	if(isChangeSize && (0 <= wSave->numRot) && (wSave->numRot <= 7))
	{
		// ����� ���� � ������ �������
		int x1 = wSave->winPos.x;
		int y1 = wSave->winPos.y;

	  // ���� ����������� ����
		int dx = wSave->mousePos.x - mouse->x;
		int dy = wSave->mousePos.y - mouse->y;
	
		// � ����� ������� �������� 
		switch(wSave->numRot)
		{
		case 0: setSize(wSave->winSize.x + dx, wSave->winSize.y + dy); setPosition(x1 - size.x, y1 - size.y); break;
		case 1: setSize(wSave->winSize.x - dx, wSave->winSize.y + dy); setPosition(x1         , y1 - size.y); break;
		case 2: setSize(wSave->winSize.x + dx, wSave->winSize.y - dy); setPosition(x1 - size.x, y1         ); break;
		case 3: setSize(wSave->winSize.x - dx, wSave->winSize.y - dy); setPosition(x1         , y1         ); break;

		case 4: setSize(wSave->winSize.x     , wSave->winSize.y + dy); setPosition(x1 - size.x, y1 - size.y); break;
		case 5: setSize(wSave->winSize.x + dx, wSave->winSize.y     ); setPosition(x1 - size.x, y1 - size.y); break;
		case 6: setSize(wSave->winSize.x - dx, wSave->winSize.y     ); setPosition(x1         , y1         ); break;
		case 7: setSize(wSave->winSize.x     , wSave->winSize.y - dy); setPosition(x1         , y1         ); break;
		//���������� ��� � �����) ���� ������ ��� ��������
		//case 6: setSize(wSave->winSize.x +dx, wSave->winSize.y); setPosition(x1, y1); break;
		//case 7: setSize(wSave->winSize.x, wSave->winSize.y +dy); setPosition(x1, y1); break;
		}
		//setSize(wSave->winSize.x +dx, wSave->winSize.y +dy);
		//setPosition(x1 - dx, y1 - dy);
		/////setSize(wSave->winSize.x +dx, wSave->winSize.y +dy);
		// ���� ��������� setPosition ����� setSize, �� ����� ������ ���������� ��� :))
	}
}
// ��������� ��������� ����
void Window::changePosition()
{
	if(!isMove) return;
	setPosition(wSave->winPos.x + mouse->x - wSave->mousePos.x, 
		          wSave->winPos.y + mouse->y - wSave->mousePos.y);
}

/////////////////////////////////////////
// ��������� ��������
//////////////////////////////////////////
// ������ �� � ����
bool Window::check1(int x, int y)
{
	if(isVisible)
	{
		if((pos.x <= x) && (x <= pos.x + size.x)
		&& (pos.y <= y) && (y <= pos.y + size.y)) 
		  return true;
	}
	return false;
}
// ������ �� � ������� �����, �� ������� �������������
bool Window::check2(int x, int y)
{
	if(isVisible)
	{
		if((pos.x +1 <= x) && (x <= pos.x + size.x -1) && 
			 (pos.y +1 <= y) && (y <= pos.y + border.top -1)) 
		  return true;
	}
	return false;
}
// ������ �� ������ � ���� ��� ������������
int  Window::check3(int x, int y)
{
	if(isVisible && isChangeSize)
	{
		int k = 5;
	
		bool top   = (pos.y <= y) && (y <= pos.y + k);
		bool left  = (pos.x <= x) && (x <= pos.x + k);
		bool right = (pos.x + size.x -k <= x) && (x <= pos.x + size.x);
		bool down  = (pos.y + size.y -k <= y) && (y  < pos.y + size.y);

		if(top   && left ) return 0;
		if(top   && right) return 1;
		if(down  && left ) return 2;
		if(down  && right) return 3;
		if(top  )          return 4;
		if(left )          return 5;
		if(right)          return 6;
		if(down )          return 7;
	}
	return -1;
}
// ���������� ����� ������ ���������� ����, �� ������� ������ ������
int Window::checkButton(int x, int y)
{
	for(int i=0; i<vecButton.size(); i++)
		if(vecButton[i]->check1(x, y))
			 return i;
	return -1;
}
int Window::checkCheckBox(int x, int y)
{
	for(int i=0; i<vecCheckBox.size(); i++)
		if(vecCheckBox[i]->check1(x, y))
			 return i;
	return -1;
}
int Window::checkTextBox(int x, int y)
{
	for(int i=0; i<vecTextBox.size(); i++)
		if(vecTextBox[i]->check1(x, y))
			 return i;
	return -1;
}
//////////////////////////////////////////
// ���������� ���������
/////////////////////////////////////////
void Window::addItem(int type, sf::String filename, vec2i offset, int code, int numTiles)
{
	winItem* obj = new winItem();

	switch(type)
	{
	  case WINITEM: 
			obj = new winItem();
			break;
		case WINIMAGE:   
			obj = new winImage();
			obj->init(filename, 0);  
			break;
		case WINBUTTON: 
			obj = new winButton();
			obj->init(filename, numTiles);
			break;
		case WINSLOT:
			obj = new winSlot();
			break;
		case WINCHECKBOX:  
			obj = new winCheckBox();
			obj->init(filename, numTiles);
			obj->switchFlag();
			break;
		case WINSPRITE:
			obj = new winSprite();
			obj->init(filename, numTiles);
			break;
		case WINTEXTBOX:
			obj = new winTextBox(numTiles, 20);
			obj->init(filename, numTiles);
			//obj->setSizeX(numTiles);
			break;
		case WINLABEL:
			obj = new winLabel();
			obj->setString(filename);
			break;
		default: std::cout<<"Window::addItem(type="<<type<<", ...) - type invalid"<<endl;
	}
	obj->type = type;
	obj->setWindow(this);
	obj->set(code, vec2i(offset.x + border.left, offset.y + border.top));
	switch(type)
	{
	  case WINITEM:     vecObj.push_back(obj);      break;
		case WINIMAGE:    vecObj.push_back(obj);      break;
		case WINBUTTON:   vecButton.push_back(obj);   break;
		case WINSLOT:     vecSlot.push_back(obj);     break;
		case WINCHECKBOX: vecCheckBox.push_back(obj); break;
		case WINSPRITE:   vecSprite.push_back(obj);   break;
		case WINTEXTBOX:	vecTextBox.push_back(obj);  break;
		case WINLABEL:    vecLabel.push_back(obj);    break;
	}
}
void Window::addItem(int type, sf::Image image, vec2i offset, int code, int numTiles)
{
	winItem* obj;
	switch(type)
	{
	case WINSPRITE: 
		obj = new winSprite();
		obj->type = WINSPRITE;
		obj->init(image, 0);
		break;
	}
	obj->type = type;
	obj->setWindow(this);
	obj->set(code, vec2i(offset.x + border.left, offset.y + border.top));
	switch(type)
	{
	case WINSPRITE: 
		vecSprite.push_back(obj);
		break;
	}
}

void Window::nextMessage(std::string message)
{
	cout << "nextMessage" << endl;
	for(int i = vecLabel.size()-1; i>0; i--)
		vecLabel[i]->setString( vecLabel[i-1]->getString() );

	vecLabel[0]->setString(message);
}