// CLIENT 2
#include "stdafx.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "dslnet.h"

#include "Tilemovi.h"
#include "Object1.h"
#include "eventHandlerWindows.h"
#include "Font.h"
//#include "View.h"

using namespace std;

void f1(std::string str_login, std::string str_password) { 
	registration(str_login, str_password);
}
void f2(std::string str_login, std::string str_password) { 
	if(base->getClientNum(0)->id == -1)
		login(str_login, str_password);
}
void f3(std::string str_nickname) {
	if(base->getClientNum(0)->id != -1)
		set_nickname(str_nickname);
}
void f4(std::string str_message) {
	message(str_message);
	EventHandlerWindows::Instance()->vecWindow2[2]->vecTextBox[0]->setString("");
}
void f5(std::string nickname, std::string msg) {
	message2(nickname, msg);
}

void newWindow(EventHandlerWindows* ehw, std::string str)
{
	Window* win3 = new Window(300, 128+39);
	//win3->addItem(WINBUTTON, "images/Design/Button/create.png",  vec2i(300 + 8+64, 31+64), 0, -4);
	//win3->addItem(WINBUTTON, "images/Design/Button/create2.png", vec2i(     8+128, 31+64), 1,  4);
	//win3->addItem(WINBUTTON, "images/Design/Button/create2.png", vec2i(     8+128, 31+64), 2,  4);
	//win3->addItem(WINBUTTON, "images/Design/Button/create2.png", vec2i(300+  8+64, 31+64), 3,  4);
	//win3->addItem(WINIMAGE,  "images/src/image1.png",            vec2i(         0,     0), 0,  0); 
	//win3->addItem(WINSLOT,   "images/src/winSlot1.png",          vec2i(300 +    0,     0), 0,  0);

	win3->addItem(WINIMAGE,  "images/src/login.png",             vec2i(  5,  32), 0,   0); 
	win3->addItem(WINIMAGE,  "images/src/password.png",          vec2i(  5,  64), 0,   0); 
	win3->addItem(WINTEXTBOX,"images/Design/textBox1.png",       vec2i(100,  32), 0, 140);    
	win3->addItem(WINTEXTBOX,"images/Design/textBox1.png",       vec2i(100,  64), 0, 140);  
	win3->addItem(WINBUTTON, "images/src/b_registration.png",    vec2i( 15,  96), 0,  -4);
	win3->addItem(WINBUTTON, "images/src/ButtLogin.bmp",         vec2i(135,  96), 0,   4);
	win3->addItem(WINLABEL,  "",                                 vec2i(  5,   5), 0,   0);   

	win3->vecButton[1]->setFunction(&f1);
	win3->vecButton[2]->setFunction(&f2);

	win3->setText(str);
	//win3->vecButton[0]->switchLocked();
	  //win3->vecSlot[0]->setWindow(win3);
	  //win3->vecSlot[0]->add("50", sf::Color::Green);
	  //win3->vecSlot[0]->add("/",  sf::Color::Green);
	  //win3->vecSlot[0]->add("32", sf::Color::Green);
	  //win3->vecButton[0]->setFlag(false);
	ehw->add(win3);
}
void newWindowNickname(EventHandlerWindows* ehw, std::string str)
{
	Window* win = new Window(300, 100);

	win->addItem(WINIMAGE,  "images/src/nickname.png",    vec2i(  5,   8), 0,   0); 
	win->addItem(WINTEXTBOX,"images/Design/textBox1.png", vec2i(100,   8), 0, 140);    
	win->addItem(WINBUTTON, "images/src/ok.png",          vec2i( 15,  40), 0,   4);
	win->addItem(WINBUTTON, "images/src/cancel.png",      vec2i(135,  40), 0,   4);

	win->vecButton[1]->setFunction2(&f3);
	//win->vecButton[2]->setFunction(nullptr);

	win->setText(str);
	ehw->add(win);
}

void newWinChat(EventHandlerWindows* ehw, std::string str)
{
	Window* win = new Window(500, 295);
	for(int i=0; i<12; i++)
		win->addItem(WINLABEL, "", vec2i(5, 20+5+i*18), 3, 0);   
	win->addItem(WINTEXTBOX,"images/Design/textBox1.png", vec2i(5, 5), 3, 405);  
	win->vecTextBox[0]->setMaxSize(40);
	win->addItem(WINBUTTON, "images/src/ok.png",  vec2i(69, 5), 2, 4);
	win->vecButton[1]->setFunction2(&f4);
	win->setText(str);
	ehw->add(win);
}

int main()
{
	Font* font = Font::Instance();
	font->loadFromFile("fonts/myUbuntuMono-Regular.ttf");
	base = new baseClient();
	//view.reset(sf::FloatRect(0, 0, 1280, 720)); //����� ������
	//view.setSize(1280, 720);

	// <Network>
	connection(&socket); // �������� ������������ � ������� (���� � ����� � dslnet.h)
	sf::Thread thread_packets(&packets);
	thread_packets.launch();
	// </Network>

	// <Interface>
	sf::RenderWindow window(sf::VideoMode(1280, 720), "World of magic v0.2");
	Keyboard kb;
	Mouse& mouse = Mouse::Instance();
	mouse.setWindow(&window);
	mouse.setMouseCursorVisible(false);
	
	EventHandlerWindows* EHW = EventHandlerWindows::Instance();
	EHW->data();
	EHW->renderWindow = &window;

	//EHW->add(new Window(300, 400));
	//EHW->add(new Window(400, 200));
	newWindow(EHW, "registration/login");
	newWindowNickname(EHW, "nickname");
	newWinChat(EHW, "���");
	// </Interface>

	Object1* p;
	p = new Object1();
	p->loadFromFile("tileset/mag.png");
	p->image.setOriginNum(5);
	p->image.data_frames(3,4);
	p->image.setScale(2,2);
	p->setPosition(100, 100);

	bool focus = true;
	float time = 0;
	sf::Clock clock;

	while (window.isOpen())
	{
		time = clock.getElapsedTime().asSeconds();
		clock.restart();

		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				sendPacket(getPacket(20));
				logout();
				socket.disconnect();
				window.close();
			} else if(event.type == sf::Event::KeyPressed){
				EHW->eventKey(1, event.key.code);
			} else if(event.type == sf::Event::KeyReleased){
				EHW->eventKey(3, event.key.code);
			}	

			if(event.type == sf::Event::MouseButtonPressed){
				EHW->eventMouse(1, event.mouseButton.button);
			} else if(event.type == sf::Event::MouseButtonReleased){
				EHW->eventMouse(3, event.mouseButton.button);
			}

			else if (event.type == sf::Event::LostFocus)
				focus = false;
			else if (event.type == sf::Event::GainedFocus)
				focus = true;
		}
		if(focus)
		{
			if(keyboard.getEventButton(sf::Keyboard::Num3) == 1) EHW->switchVisibleWin(2);
			if(keyboard.getEventButton(sf::Keyboard::Num2) == 1) EHW->switchVisibleWin(1);
			if(keyboard.getEventButton(sf::Keyboard::Num1) == 1) EHW->switchVisibleWin(0);
		}
		mouse.behavior();
		if(focus == true && mouse.getEventButton(sf::Mouse::Right) == 3)
		{
			vec2i vec;
			vec.x = base->getClientNum(0)->obj.getPosition().x;
			vec.y = base->getClientNum(0)->obj.getPosition().y;
			newpos(vec, mouse.pos);
			//p->newTargetMove(1, mouse.pos.x, mouse.pos.y);
		}

		base->behavior(time);

		EHW->eventMouse(-1, sf::Mouse::Left);
		EHW->behavior(time);

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

		const int HEIGHT_MAP = 25;//������ ����� ������
		const int WIDTH_MAP = 65;//������ ����� ������ 
 
		 sf::String qmap[HEIGHT_MAP] = {
			"00000000000000000000000000000000000000000000000000000000000000000",
			"0                                                               0",
			"0   s                                                           0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"0                                                               0",
			"00000000000000000000000000000000000000000000000000000000000000000",
		};
		sf::Image map_image;//������ ����������� ��� �����
		map_image.loadFromFile("images/map.png");//��������� ���� ��� �����
		sf::Texture map;//�������� �����
		map.loadFromImage(map_image);//�������� �������� ���������
		sf::Sprite s_map;//������ ������ ��� �����
		s_map.setTexture(map);//�������� �������� ��������

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

		window.clear(sf::Color::White);

		for (int i = 0; i < HEIGHT_MAP; i++)
		for (int j = 0; j < WIDTH_MAP; j++)
		{
			if (qmap[i][j] == ' ')  s_map.setTextureRect(sf::IntRect(0, 0, 64, 64)); //���� ��������� ������ ������, �� ������ 1� ���������
			if (qmap[i][j] == 's')  s_map.setTextureRect(sf::IntRect(64, 0, 64, 64));//���� ��������� ������ s, �� ������ 2� ���������
			if ((qmap[i][j] == '0')) s_map.setTextureRect(sf::IntRect(128, 0, 64, 64));//���� ��������� ������ 0, �� ������ 3� ���������
			s_map.setPosition(j * 64, i * 64 );//�� ���� ����������� ����������, ��������� � �����. �� ���� ������ ������� �� ��� �������. ���� ������, �� ��� ����� ���������� � ����� �������� 32*32 � �� ������ ���� �������
			window.draw(s_map);//������ ���������� �� �����
		}
		
		vec2f PosCam = base->getClientNum(0)->obj.getPosition();

		//view.setCenter(PosCam);
		//viewmap(time);//������� ���������� �����, �������� �� ����� sfml
		//changeview();//������������� � ������� ����
		//window.setView(view);//"��������" ������ � ���� sfml

		base->draw(&window);
		EHW->draw();
		
		mouse.draw();
		window.display();
	}

	// ������� ����� ��������� �������
	thread_packets.terminate(); 
	
	return 0;
}