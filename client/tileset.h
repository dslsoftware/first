//Tileset.h
//v2.0 07.01.2017

#ifndef DSL_TILESET_H
#define DSL_TILESET_H

#include "stdafx.h"
#include "Image.h"
#include "myMath.h"

class Tileset : public Image
{
public:
  bool flag1; // ����� �� �����, ���� �������? (true - ��������)
  bool flag2; // ��� ����� ������� ��������?
  bool flag3; // ����� �� ���������� �� ����� ������ (��� ���������� ���������)

  int numFrameWcur; // ������� ����� ����� �� ������
  int numFrameHcur; //                        ������
  int numFrameWmax; // ������������ ���������� ������ �� ������
  int numFrameHmax; //                                   ������

	int numFrameWmax2; // ������������ ������� ���������� ������ �� ������
  int numFrameHmax2; //                                           ������

  int frameW; //  ???
  int frameH; //  ???

	int numFrame;
public:
  Tileset();
	Tileset(sf::String filename);
	//

	void data_frames(int number_frame_width, int number_frame_height); 
	void data_frames_blackboard(int number_frame_width, int number_frame_height);

  void data();
  bool update_frame();

  void setNumFrameWcur(int number); //
  void setNumFrameHcur(int number); //
	void setNumFrameRand();
	void setNumFrameWnext();

  int getNumFrameWcur(); //
  int getNumFrameHcur(); //
  
  void setFlag1(bool flag); //
  void setFlag3(bool flag); //
  //bool getFlag1();
  //bool getFlag3();

	int getAlphaPixelTileset(unsigned int x, unsigned int y) const;
};

#endif DSL_TILESET_H

//