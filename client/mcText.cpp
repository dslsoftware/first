#include "mcText.h"

void mcText::data()
{
	dx = 0;
	dy = 0;
	charSize = 16;
	index = sf::Vector2i(0, 0);
	setLimit(false, 0);
	add("");
}

mcText::mcText()
{
	data();
}

mcText::~mcText(void) { }

int mcText::findCharacterSize(sf::Text text, int index) const
{
	if(index < 0) return 0;
	return text.findCharacterPos(index+1).x - text.findCharacterPos(index).x;
}

// [���������] ���������� ������ text � �������� ������������ ������ ������
sf::Vector2i mcText::scan(sf::Vector2i mousePos)
{
	for(int i=0; i<vecText.size(); i++)
	{
		// �������� ������� ������
		sf::FloatRect vec = vecText[i].getGlobalBounds();
		// ������� � ������� ������� ������
		cout<<vec.left<<","<< vec.top<<","<<vec.width<<","<<vec.height<<endl;
		// ���� ����� �������� �� ������
		if(vec.top <= mousePos.y && mousePos.y <= vec.top + vec.height)
		{
			// ���� ����� �������� �� ������
			if(vec.left <= mousePos.x && mousePos.x <= vec.left + vec.width)
			{

				int dx = mousePos.x - vecInfo[i]->dx; // �������� ���� ������������ ������ ������
				sf::Vector2f vec;
				int n = 0;
				while(vec.x <= dx)
				{
					vec = vecText[i].findCharacterPos(n);
					vec.y = vecText[i].findCharacterPos(n-1).x;
					n++;
					if(n>100) 
						break;
				}
				index.x = n-2;
				vec = sf::Vector2f(vecInfo[i]->dx + vec.y, vec.x - vec.y);
				return sf::Vector2i(vec.x, vec.y);
			}
		}
	}
	return sf::Vector2i(0, 0);
}
// ���������� ����������� �� �����������
void mcText::setLimit(bool flag, int num)
{
	isLimitW = flag;
	limitW = num;
}
// �������� � ���������� ������
void mcText::add0(int index, sf::String str)
{
	int size_ = vecText.size()-1;
	if(size_ >= 0 && str != "")
	{
		vecInfo[size_]->str.insert(index, str);
		//vecInfo[size_]->w += str.getSize()*10;
		vecText[size_].setString(vecInfo[vecText.size()-1]->str);
	}
}
// �������� ��������� ������
void mcText::add(sf::String str)
{
	add(str, sf::Color::Black);
}
// ������� ��������� ������� ������
void mcText::add(sf::String str, sf::Color color)
{
	mcTextInfo* t = new mcTextInfo();
	t->color = color;
	t->str = str;

	// ������ �����
	sf::Text text;
	end = &text;
	text.setFont(*Font::Instance());
	text.setCharacterSize(charSize);
	text.setColor(t->color);
	text.setString(t->str);

	// ��� �� ��������� � ������, � �� ��� ������ ����� ��������
	t->w = text.getLocalBounds().width;
	t->h = text.getLocalBounds().height;
	t->dx = dx;
	dx += t->w;
	t->dy = dy;
		
	// ���� �������� ����������� �� W
	if(isLimitW && (t->dx + t->w > limitW))
	{
		enter();
		t->dx = dx;
		t->dy = dy;
	}

	text.move(t->dx, t->dy);

	vecInfo.push_back(t);
	vecText.push_back(text);
}
// ���������� �������� �������������� ������
void mcText::add2(sf::String str)
{
	std::vector<sf::String> vecWords;
	std::vector<sf::String> vecStrings;
	sf::Text text = vecText[0];
	
	//����� ����� �� �����
	vecWords = scanWords(str);
	//������������ ����� �� ������� (�������� �����������)
	vecStrings = scanString(limitW, vecWords, &text);
	//��������� ������ � �������� ������ �����
	for(int i=0; i<vecStrings.size(); i++)
	{
		add(vecStrings[i]);
		enter();
	}
};
// ������� �� ��������� ������
void mcText::enter()
{
	dx = 0;
	dy += charSize;
	if(vecInfo.size() != 0)
		add("",vecInfo[vecInfo.size()-1]->color);
}
// �������� ������� �� �������
int mcText::backspace(int index)
{
	if(index < 0) return 0;
	int size = vecText.size()-1; // ���������� �������
	if(size < 0)return 0; // ���� ���� �������, �������
	if(vecInfo[size]->str.isEmpty()) return 0; // ���� ����� ����, �������
	int sizeX = findCharacterSize(vecText[size], index);
	vecInfo[size]->str.erase(index); // ������� ���� ������
	end->setString(vecInfo[size]->str); // ������������� ������ � �����
	return sizeX;
}
// �������� ������� � �����
void mcText::backspace()
{
	int size = vecText.size()-1; // ���������� �������
	if(size < 0)return; // ���� ���� �������, �������
	if(vecInfo[size]->str.isEmpty()) return; // ���� ����� ����, �������
	vecInfo[size]->str.erase(vecInfo[size]->str.getSize()-1); // ������� ���� ������
	//vecInfo[size]->w -= 10; 
	end->setString(vecInfo[size]->str); // ������������� ������ � �����
}
void mcText::space()
{
	add0(end->getString().getSize()-1, " ");
}
// �������� �� value �������� �� �����������
void mcText::space(int value)
{
	dx += value;
}
// �������� �� value �������� �� ���������
void mcText::spaceY(int value)
{
	dy += value;
}
// ����������� ���� ����� �� dx:dy
void mcText::move(float dx, float dy)
{
	for(std::vector<sf::Text>::iterator it = vecText.begin(); it != vecText.end(); it++)
		(*it).move(dx, dy);
}
// ����������� ���� ����� � ���������� x:y
void mcText::setPosition(float x, float y)
{
	for(int i=0; i<vecText.size(); i++)
		vecText[i].setPosition(x + vecInfo[i]->dx, y + vecInfo[i]->dy);
}
void mcText::setPosition(sf::Vector2f pos)
{
	for(int i=0; i<vecText.size(); i++)
		vecText[i].setPosition(pos.x + vecInfo[i]->dx, pos.y + vecInfo[i]->dy);
}
void mcText::setPosition(sf::Vector2i pos)
{
	for(int i=0; i<vecText.size(); i++)
		vecText[i].setPosition(pos.x + vecInfo[i]->dx, pos.y + vecInfo[i]->dy);
}
// ���������� �� �������� ����
void mcText::draw(sf::RenderWindow* window)
{
	window->draw(imgPipe.sprite);
	for(std::vector<sf::Text>::iterator it = vecText.begin(); it != vecText.end(); it++)
		window->draw(*it);
}
// ���������� ������ ���� ��� �������� | ��������� �����
std::vector<sf::String> mcText::scanWords(sf::String str)
{
	std::vector<sf::String> vecStr;
	sf::String buff;

	for(int i=0; i<str.getSize(); i++)
		if(str[i] != ' ')
			buff += str[i];
		else
			if(buff != "")
			{
				vecStr.push_back(buff);
				buff = "";
			}
	vecStr.push_back(buff);
	return vecStr;
}
// ���������� ������ ������� ����� | ��������� ������ � ������ ���� � ����������� ����� (� ������� � ��������)
std::vector<sf::String> mcText::scanString(int w, std::vector<sf::String> vecWords_, sf::Text* text)
{
	std::vector<sf::String> vecStr;
	sf::String buff;
	sf::String buff2;

	for(int i=0; i<vecWords_.size(); i++)
	{
		buff = buff2;
		if(buff2 == "")
			buff2 += vecWords_[i];
		else
			buff2 += " " + vecWords_[i];
		text->setString(buff2); // ������ ������ ����� �������� ������ ������
		if(text->getLocalBounds().width > w)
		{// ���� ��� ���������� ����� ������ ����� ������ ������
			vecStr.push_back(buff); // ��������� � ������ ����� �����1 (�� �������� ���������� �����)
			buff = "";           // ������ �����1
			buff2 = vecWords_[i]; // � �����2 ���������� ��������� ����� (�.�. ������� �� ����. ������)
		}
	}
	vecStr.push_back(buff2);
	return vecStr;
}
// ��������� �������� �����
void mcText::clear()
{
	vecInfo.clear();
	vecText.clear();
	data();
}
int mcText::getW(int num)
{
	if(num < vecInfo.size())
		return vecInfo[num]->w;
	return -1;
}