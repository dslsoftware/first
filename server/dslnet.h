#pragma once
#include <iostream>
#include "SFML/Network.hpp"
#include "myMath.h"
using namespace std;

struct Client
{
	int id;
	bool online;
	std::string login;
	std::string password;
	std::string nickname; 
	int numTileset; 
	sf::Vector2i pos;
	sf::Vector2i pos2;
	Client() : id(0), online(false), login(""), password(""), nickname(""), numTileset(0) { }
};

struct s31032017n1
{
	sf::TcpSocket* socket;
	sf::Packet packet;
	s31032017n1(sf::TcpSocket* socket, sf::Packet packet) : socket(socket), packet(packet) { }
};

struct s02042017n1
{
	sf::TcpSocket* socket;
	Client* client;
	s02042017n1(sf::TcpSocket* socket, Client* client) : socket(socket), client(client) { }
};

struct baseClient
{
	std::vector<Client*> vecClient;

	Client* addClient(std::string login, std::string password)
	{
		Client* client = new Client();
		client->login = login;
		client->password = password;
		client->id = vecClient.size();
		vecClient.push_back(client);
		return client;
	}
	Client* search_client_to_login(std::string login)
	{
		for(int i=0; i<vecClient.size(); i++)
			if(vecClient[i]->login == login)
				return vecClient[i];
		return nullptr;
	}
	Client* search_client_to_id(int id)
	{
		for(int i=0; i<vecClient.size(); i++)
			if(vecClient[i]->id == id)
				return vecClient[i];
		return nullptr;
	}
};

void coutPacket(sf::Packet);
sf::Packet createPacket(sf::Uint16 code);
sf::Packet createPacket(sf::Uint16 code, int id);
sf::Packet createPacket(sf::Uint16 code, int id, std::string str1);


void send_mass(sf::Packet packet, std::vector<sf::TcpSocket*> vec);

//