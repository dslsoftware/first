//Keyboard.h
//v1.0 16.12.2015
#include "stdafx.h"

#ifndef DSL_KEYBOARD_H
#define DSL_KEYBOARD_H

class Keyboard
{
private:
  bool mass[101];
	void data();
public:
	Keyboard();
	~Keyboard();
 	// ������ ������� ���������� �������, 
	// ������� ��������� � ����������� ��������, 
	// � ������� ������ �������
	// 0 - ��� �������         (�����������)
	// 1 - ������� �������     (��������)
	// 2 - ����������� ������� (�����������)
	// 3 - ���������� �������  (��������)
	int getEventButton(sf::Keyboard::Key key);
};

static Keyboard keyboard;

#endif  DSL_KEYBOARD_H