//myMath.h
//v2.1 11.04.2017

#include "stdafx.h"

#ifndef DSL_MYMATH_H
#define DSL_MYMATH_H

typedef sf::Vector2u  vec2u;
typedef sf::Vector2i  vec2i;
typedef sf::Vector2f  vec2f;

typedef sf::IntRect   vec4i;
typedef sf::FloatRect vec4f;


// ������
// ����������� - ����� max - ����� �������� �������
// start() - ��������� �������
// update(time) - ���������� false ���� ����� ����������.
struct Timer
{
	bool flag; // true - ����������
	float max;     // ������������ �����
	float current; // ������� �����
	float remnant; // ���������� �����
	Timer(float max_time)
	{
		flag = false;
		max = max_time;
		current = 0;
		remnant = 0;
	}
	void start()
	{
		flag = false;
		current = 0;
		remnant = 0;
	}
	// false - ����� �����
	bool update(float time)
	{
		if(current < max)
		{
			current += time;

			if(current < max)
			{
				return true;
			} 
			else 
			{
				flag = true;
			  remnant = current - max; // ���������� ����� = ������� - ������������
				current = max;           // ������� ����� = ������������
				return false;
			}
		} 
		return false; // ����� �����������
	}
};

//

sf::Color xColor(sf::Color color);
struct myVector3i
{
	int numGMap;
	int mumMap;
	int numCell;
};

double const PI = 3.14159265;

int myMin(int n1, int n2);
int myMax(int n1, int n2);
// ���������� �������� ������ � �������� �� �������� ����������� � ��������
float myCos(float angle);
float mySin(float angle);
float myMod(float number); // ������ �����
int myMod(int number);
// ===== ===== =====
int myRand1(int n1, int n2);
int myRand2(float percent);
float n2(float n);

float distance(float  x1, float  y1, float  x2, float  y2);
float distance2(sf::Vector2f pos1, sf::Vector2f pos2);

// d_angle ����������, ��� ����� ����� ����� ��������� ������� 1(��)
// ����� �������������� � ������� 2(�)
// dx = x2(�) - x1(��)
// dy = y2(�) - y1(��)
float d_angle(float dx, float dy);
// d_angle ����������, ��� ����� ����� ��������� ������2 ������������ �������1
float d_angle(float x1, float y1, float x2, float y2);
float d_angle(sf::Vector2f pos1, sf::Vector2f pos2);

float d_angle(sf::Vector2f a, sf::Vector2f b, sf::Vector2f p);
// return true if ��� ���������� ������������� 
// ������ ����� �� ����������� ����� ������������ ����� (h = 0)
bool HitTestPointCircle(float x1, float y1, float h1, float x2, float y2, float h2);

sf::String intToString(int value);
int stringToInt(std::string str);
// ���������� ������ ���� ��� �������
// ��������� ������ � �������
std::vector<sf::String> scanWords(sf::String str);

// ���������� ������ ������� �����
// ��������� ������ � ������ ���� � ����������� ����� (� ������� � ��������)
std::vector<sf::String> scanString(int w, std::vector<sf::String> vecWords_, sf::Text* text);

#endif DSL_MYMATH_H
//













