//Item.cpp
//v0.2.1 08.01.2017
#include "Item.h"
Item::Item(void) { }
Item::~Item(void) { }
// ��������� ��������
bool Item::loadFromFile(sf::String filename)
{
	if(image.loadFromFile(filename))
	{
		texture.loadFromImage(image);
		sprite.setTexture(texture);
		size = vec2i(image.getSize().x, image.getSize().y);
		sprite.setTextureRect(sf::IntRect(0, 0, size.x, size.y));
		return true;
	}
	return false;
}
// ���������� ��������
void Item::setImage(sf::Image img)
{
	image = img;
	sf::Vector2f vec = sprite.getPosition();
	texture.loadFromImage(img);
	sprite.setTexture(texture);
	size = vec2i(img.getSize().x, img.getSize().y);
	sprite.setTextureRect(sf::IntRect(0, 0, size.x, size.y));
	sprite.setPosition(vec);
}
// ���������
void Item::draw(sf::RenderWindow* window)
{
	window->draw(sprite);
}
// ���������� ����������
void Item::setPosition(vec2f position)
{
	sprite.setPosition(position);
	pos = vec2i(position.x, position.y);
}
void Item::setTextureRect(vec4i vec)
{
	sprite.setTextureRect(vec);
	size.x = vec.width;
	size.y = vec.height;
}
// ��������� true, ���� �������� ���������� ����������� sprite
bool Item::check1(int x, int y)
{
	if((sprite.getPosition().x <= x) && (x <= sprite.getPosition().x + size.x)
	&& (sprite.getPosition().y <= y) && (y <= sprite.getPosition().y + size.y)) 
	  return true;
	return false;
}