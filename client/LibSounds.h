#pragma once
#include "stdafx.h"

class LibSounds
{
public:
	int gVolume;
	std::vector<std::string>      vec_filename_sounds;
	std::vector<sf::SoundBuffer*> vec_soundbuffers;
	std::vector<sf::Sound*>			  vec_sounds;

	std::vector<std::string>		  vec_filename_musics;
	std::vector<sf::Music*>			  vec_musics;
public:
	static LibSounds* Instance(){
		static LibSounds* libSounds = new LibSounds();
		return libSounds;
	}
	//LibSounds() { data(); }
	//~LibSounds() { }
	void data()
	{
		gVolume = 100;
		data_sounds();
		data_musics();
		load_sounds();
	}
	void data_sounds()
	{
		vec_filename_sounds.push_back("sounds/InfClick.wav");
	}
	void load_sounds()
	{
		// �������� �� ���������
		for(int i=0; i<vec_filename_sounds.size(); i++)
		{
			sf::SoundBuffer* buffer = new sf::SoundBuffer();
			buffer->loadFromFile( vec_filename_sounds[i] );
			vec_soundbuffers.push_back(buffer);

			sf::Sound* sound = new sf::Sound();
			sound->setBuffer(*buffer);
			vec_sounds.push_back(sound);
		}
	}
	void data_musics()
	{
		vec_filename_musics.push_back("sounds/Flyff-Flaris_2.wav");
		vec_filename_musics.push_back("sounds/Flyff-General_1.mp3");
	}
	int play_sound(int num)
	{
		if(num < vec_sounds.size())
			vec_sounds[num]->play();
		return 0;
	}
	int play_music(int num, bool loop)
	{
		if(num >= vec_filename_musics.size() || num < 0) return -1;

		sf::Music* music = new sf::Music();
		if( music->openFromFile(vec_filename_musics[num]) == false ) {
			cout << "Error music " << &vec_filename_musics[num] << endl;
			return -1;
		}

		music->setLoop(loop);
		music->play();

		vec_musics.push_back(music);
	}
	void setGVolue(int volume)
	{
		gVolume = volume;
		for(int i=0; i<vec_sounds.size(); i++)
			vec_sounds[i]->setVolume(volume);

		for(int i=0; i<vec_musics.size(); i++)
			vec_musics[i]->setVolume(volume);
	}
	void setVolumeSound(int num, int volume)
	{
		if(volume < 0) volume = 0;
		if(volume > 100) volume = 100;
		if(num < vec_sounds.size())
			vec_sounds[num]->setVolume(volume);
	}
	void setVolumeMusic(int num, int volume)
	{
		if(volume < 0) volume = 0;
		if(100 < volume) volume = 100;
		if(num < vec_musics.size())
			vec_musics[num]->setVolume(volume);
	}
};