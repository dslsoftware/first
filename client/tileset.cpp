//Tileset.cpp
//v2.0 07.01.2017
#include "Tileset.h"

Tileset::Tileset()
{
  data();
}
Tileset::Tileset(sf::String filename)
{
	data();
	loadFromFile(filename);
}


void Tileset::data_frames(int number_frame_width, int number_frame_height)
{
	numFrame = number_frame_width*number_frame_height;

  numFrameWmax = number_frame_width;
	numFrameHmax = number_frame_height;
	data_frames_blackboard(numFrameWmax, numFrameHmax);
	numFrameWcur = 1;
	numFrameHcur = 1;

	int width  = getSize().x;
	int height = getSize().y;

	// ���� ������ � ������ �������� �� 1 ��������, ����������� ������ � ������ �����
	if(number_frame_width > 1)  frameW = int(width/number_frame_width);   else frameW = width;
	if(number_frame_height > 1) frameH = int(height/number_frame_height); else frameH = height;	
	// ������������� �������� � ����
	sprite.setOrigin(width/numFrameWmax - frameW/2, height/numFrameHmax - frameH/2);
  sprite.setTextureRect(sf::IntRect((numFrameWcur-1)*frameW, (numFrameHcur-1)*frameH, frameW, frameH));
}
void Tileset::data_frames_blackboard(int number_frame_width, int number_frame_height)
{
	// ������� ����� ������� ������� Tileset-�
	// ������������ ��� ��������, � ������� ���� ������ ������
	numFrameWmax2 = number_frame_width;
	numFrameHmax2 = number_frame_height;
}

void Tileset::data()
{
  flag1 = false;
  flag2 = false;  
	flag3 = false; 

  numFrame = 1; // ����� ���������� ������

  numFrameWcur = 1; // ������� ���������� ������ �� ������
  numFrameHcur = 1; //                              ������
  numFrameWmax = 1; // ������������ ���������� ������ �� ������
  numFrameHmax = 1; //                                   ������

	frameW = getSize().x; //
	frameH = getSize().y; //
}

bool Tileset::update_frame()
{
	// ���������� �����
  if(flag1 == true)
  {// ���� �� ������������ �������?
		if(flag2 == true)
		{// ���� ��� �����...
			numFrameWcur--;
			if(numFrameWcur < 1)
			{// ���� ������� ���� ������ 1
				flag2 = false;
				numFrameWcur = 2;
			}
		} 
		else 
		{// ���� ��� �����
			numFrameWcur++;
			if(numFrameWcur > numFrameWmax2)
			{
				flag2 = true;
				numFrameWcur = numFrameWmax2-1;
			}
		}
  } 
	else 
	{// ���� �� ���� ��������������� � �����
		numFrameWcur++;
		if(flag3 == true)
		{
			if(numFrameWcur > numFrameWmax2)
			{
				numFrameHcur +=1;
				numFrameWcur  =1;
				if(numFrameHcur > numFrameHmax2)
				{
					numFrameHcur = 1;
					return false;
				}
			}
		}
		else
		{
			if(numFrameWcur > numFrameWmax2)
			{
			  numFrameWcur = 1; // ���� ���� �� �������, ��������� �� ����1
				//return false; // �� 08.11.2016
			}
		}
	}

  // ������ ���� ���������
  sprite.setTextureRect(sf::IntRect((numFrameWcur-1)*frameW, (numFrameHcur-1)*frameH, frameW, frameH));
	return true;
}

// ��������� ����� �� ������
void Tileset::setNumFrameWcur(int number)
{
  if(number > numFrameWmax)      
    numFrameWcur = numFrameWmax;
  else if(number < 1)
	    numFrameWcur = 1;
    else 
	    numFrameWcur = number;
  sprite.setTextureRect(sf::IntRect((numFrameWcur-1)*frameW, (numFrameHcur-1)*frameH, frameW, frameH));
}
void Tileset::setNumFrameWnext()
{
	setNumFrameWcur(numFrameWcur+1);
}
// ��������� ����� �� ������
void Tileset::setNumFrameHcur(int number)
{
  if(number > numFrameHmax)
    numFrameHcur = numFrameHmax;
  else if(number < 1)
	  numFrameHcur = 1;
  else 
	  numFrameHcur = number;
  sprite.setTextureRect(sf::IntRect((numFrameWcur-1)*frameW, (numFrameHcur-1)*frameH, frameW, frameH));
}
void Tileset::setNumFrameRand()
{
	setNumFrameWcur(myRand1(1, numFrameWmax));
	setNumFrameHcur(myRand1(1, numFrameHmax));
}

// ���������� ������� ����� ����� �� ������
int Tileset::getNumFrameWcur()
{
  return numFrameWcur;
}

// ���������� ������� ����� ����� �� ������
int Tileset::getNumFrameHcur()
{
	return numFrameHcur;
}

void Tileset::setFlag1(bool flag)
{
  flag1 = flag;
}
void Tileset::setFlag3(bool flag)
{
  flag3 = flag;
}

// ������� ���������� ������������ ������� � �������� ���� Tileset-�
int Tileset::getAlphaPixelTileset(unsigned int x, unsigned int y) const
{
	int xx = sprite.getTextureRect().left + x;
	int yy = sprite.getTextureRect().top  + y;
	
	return getAlphaPixel(xx, yy);
}
//