//main.cpp
// SERVER
#pragma comment(lib, "sfml-network.lib")
#include <iostream>
#include <SFML/Network.hpp>
#include "dslnet.h"
#include "Game.h"

using namespace std;
const unsigned short PORT = 621;
//const std::string IPADDRESS("176.65.120.47"); // ��������� �����
//const std::string IPADDRESSL("10.35.111.204"); // ��������� �����
//const std::string IPADDRESSL("192.168.0.183"); // ��������� �����


sf::TcpListener listener;
sf::SocketSelector selector;
std::vector<sf::TcpSocket*> clients;


struct Sockets
{
	std::vector<s02042017n1*> vecSockets; // ������ + �������
	void add(sf::TcpSocket* socket, Client*  client)
	{
		s02042017n1* s = new s02042017n1(socket, client);
		vecSockets.push_back(s);
	}
	void send_mass(sf::Packet packet)
	{
		for(int i=0; i<vecSockets.size(); i++)
		{
			if(vecSockets[i]->client != nullptr)
				vecSockets[i]->socket->send(packet);
		}
	}
	// �������� ��������� �� id �������
	Client* getClient(int index)
	{
		if(index < vecSockets.size())
			return vecSockets[index]->client;
		return nullptr;
	}
	void setClient(int index, Client* client)
	{
		if(index < vecSockets.size())
			vecSockets[index]->client = client;
	}
	void disconnect(Client* client)
	{
		for(int i=0; i<vecSockets.size(); i++)
		{
			if(vecSockets[i]->client == client)
			{
				disconnect_to_index(i);
				return;
			}
		}
	}
	
	void disconnect(int id)
	{
		for(int i=0; i<vecSockets.size(); i++)
		{
			if(vecSockets[i]->client->id == id)
			{
				cout << "socket disconnect id="<< id << endl;
				disconnect_to_index(i);
				return;
			}
		}
	}
	
	int size() { return vecSockets.size(); }
	// ��������� ����� � ������ ������� Offline � ������� ����������
 	void disconnect_to_index(int index)
	{
		if(index >= size()) return;
		vecSockets[index]->socket->disconnect();
		vecSockets[index]->client->online = false;
		vecSockets.erase(vecSockets.begin() + index);
	}

	void erase(int index) { vecSockets.erase(vecSockets.begin()+index); }
};
std::vector<s31032017n1*> listOfPackets;
Sockets* sockets; // ������ ������ � �������(����� ��� �������)
baseClient base; // �������

struct LibPackets
{
	LibPackets() { }
	void registration(s02042017n1* str, sf::Packet packet)
	{	
		std::string login;
		std::string password;
		int id = -1;
		packet >> id >> login >> password;
		cout<<"registration("<<id<<","<<login<<","<< password<<")=";

		// ���� �� ����
		if(base.search_client_to_login(login) != nullptr)
		{
			cout << "false" << endl;
			listOfPackets.push_back(new s31032017n1(str->socket, createPacket(2))); // ��������� � ���, ��� ����� �����
		} else {
			cout << "true" << endl;
			Client* c = base.addClient(login, password); // ��������� � ���� �������
			c->pos.x = 100; //myRand1(0, 640);
			c->pos.y = 100; //myRand1(0, 480);
			listOfPackets.push_back(new s31032017n1(str->socket, createPacket(3, c->id))); // ��������� �� �������� �����������
		}
	}
	// ���� � ������� ������
	void login(s02042017n1* str, sf::Packet packet)
	{
		std::string login;
		std::string password;
		int id = -1;
		packet >> id >> login >> password;
		cout<<"login("<<id<<","<<login<<","<< password<<")=";
		
		Client* client = base.search_client_to_login(login); // ���� ������� �� ID

		if(client == nullptr)
		{
			cout << "false : error login" << endl;
			listOfPackets.push_back(new s31032017n1(str->socket, createPacket(12, -1)));
		} else if(client->password != password) {
			cout << "false : error password" << endl;
			listOfPackets.push_back(new s31032017n1(str->socket, createPacket(13, client->id)));
		} else if(client->online == true) {
			cout << "false : client online" << endl;
			listOfPackets.push_back(new s31032017n1(str->socket, createPacket(14, client->id)));
		} else {
			cout << "true : id = " << client->id << endl;
			str->client = client; // ��������� � �������
			client->online = true;
			listOfPackets.push_back(new s31032017n1(str->socket, createPacket(11, client->id)));
			clients_pos_all(str); // 50
			connect_client(client); // 48
		}
	}
	// ����� �� ������� ������ (flag == true - 100% �����)
	void logout(s02042017n1* str, sf::Packet packet)
	{
		int id = -1;
		packet >> id;
		cout<<"logout("<<id<<")=";
		bool flag = true;
		if(str->client == nullptr) {
			cout << "false" << endl;
			listOfPackets.push_back(new s31032017n1(str->socket, createPacket(32)));
		}	else if(flag == false) {
			cout << "..." << endl;
			// ������ �� ����������... SAO
		} else {
			cout << "true" << endl;
			str->client->online = false;
			listOfPackets.push_back(new s31032017n1(str->socket, createPacket(31)));
			disconnect_client(str->client); // 49
		}
	}

	void set_nickname(s02042017n1* str, sf::Packet packet)
	{
		int id = -1;
		std::string nickname;
		int numTileset;
		packet >> id >> nickname >> numTileset;
		cout<<"set_nickname("<<id<<","<<nickname<<");" << endl;
		packet.clear();
		if(id < 0) {
			listOfPackets.push_back( new s31032017n1(str->socket, createPacket(32)) );
		} else if(str->client == nullptr) {
			listOfPackets.push_back( new s31032017n1(str->socket, createPacket(33)) );
		} else {
			str->client->nickname = nickname;
			str->client->numTileset = numTileset;
			sf::Packet packet = createPacket(31, id, nickname);
			packet << numTileset;
			listOfPackets.push_back( new s31032017n1(nullptr, packet) );
		}
	}

	// ���������� ���� � �����������/���������� �������
	void connect_client(Client* client)
	{
		sf::Packet packet = createPacket(48, client->id);
		packet << client->nickname << client->pos.x << client->pos.y;
		cout << "all packet48(id=" << client->id <<","
			<< client->nickname << ","
			<< client->pos.x << ":" <<client->pos.y << endl;
		listOfPackets.push_back(new s31032017n1(nullptr, packet));
	}
	// ��������� �������
	void disconnect_client(Client* client)
	{
		if(client == nullptr) return;
		cout << "disconnect_client id=" << client->id << endl;
		listOfPackets.push_back(new s31032017n1(nullptr, createPacket(49, client->id)));
	}
	// ���������� ������ � ���� ��������� ��������
	void clients_pos_all(s02042017n1* str)
	{
		if(str->client == nullptr) return;
		cout << "pdate_position(...) id="<<str->client->id << endl;
		std::vector<s02042017n1*> vec; // ������ ������ ��������

		for(int i=0; i<sockets->vecSockets.size(); i++)
			if(sockets->vecSockets[i]->client != nullptr)
				vec.push_back(sockets->vecSockets[i]);

		sf::Packet packet = createPacket(50, vec.size());

		for(int i=0; i<vec.size(); i++)
		{
			packet << vec[i]->client->id 
						 << vec[i]->client->nickname
						 << vec[i]->client->pos.x 
						 << vec[i]->client->pos.y;
		}

		listOfPackets.push_back(new s31032017n1(str->socket, packet));
	}
	// ���������� ���� � ����� ����������� ������
	void nextpos(s02042017n1* str, sf::Packet packet)
	{
		if(str->client == nullptr) return;
		cout << "nextpos(...) id=" << str->client->id << endl;
		vec2i pos1;
		vec2i pos2;
		packet >> pos1.x >> pos1.y >> pos2.x >> pos2.y;
		str->client->pos = pos1;
		packet.clear();

		packet = createPacket(52, str->client->id);
		packet << pos1.x << pos1.y 
			 << pos2.x << pos2.y;
		listOfPackets.push_back(new s31032017n1(nullptr, packet));
	}

	void message(s02042017n1* str, sf::Packet packet)
	{
		std::string message;
		packet >> message;

		if(str->client != nullptr)
			listOfPackets.push_back(new s31032017n1(nullptr, createPacket(61, str->client->id, message)));
	}
};

LibPackets lib; // ������� ��������� �������


// str - ��������� ���������� ����� (� �������)
// packet - ����� � �����������
void handler_packets(s02042017n1* str, sf::Packet packet)
{
	sf::Uint16 code;
	packet >> code;
	cout << "packet(code=" << int(code) << ") ";

	//======================================================
	     if(code ==  1) lib.registration(str, packet);
	else if(code == 10)	lib.login(str, packet);
	else if(code == 20) lib.logout(str, packet);
	else if(code == 30) lib.set_nickname(str, packet);
	else if(code == 40) { } //lib.disconnect_session(str, packet);
	else if(code == 50) lib.clients_pos_all(nullptr);
	else if(code == 51) lib.nextpos(str, packet);
	else if(code == 60) lib.message(str, packet);
	//======================================================
}

bool run;
void server_accept()
{
	while(run) {
		if(selector.wait()) {
			if(selector.isReady(listener)) {
				sf::TcpSocket* client = new sf::TcpSocket;
				if(listener.accept(*client) == sf::Socket::Done) {
					std::cout << "New client connected: " << client->getRemoteAddress() << std::endl;
					sockets->add(client, nullptr);
					selector.add(*client);
				} 
				else
					delete client;
			}	else {
				for(int i=0; i<sockets->size(); i++) {

					sf::TcpSocket* client = sockets->vecSockets[i]->socket;
					if(selector.isReady(*client)) {
						sf::Packet packet;
						int status = client->receive(packet);
						if(status == sf::Socket::Done)
							handler_packets(sockets->vecSockets[i], packet);
						else if(status == sf::Socket::Disconnected) {
							std::cout << "client disconnected: " << client->getRemoteAddress() << std::endl;
							if(sockets->vecSockets[i]->client != nullptr) 
								sockets->vecSockets[i]->client->online = false;
							lib.disconnect_client(sockets->vecSockets[i]->client); // 49
							selector.remove(*client);
		
							sockets->erase(i);

							//listOfPackets.push_back(new s31032017n1(client, createPacket(31)));


							//sockets->erase(i);
							break;
						}
					} 
				}
				if(listOfPackets.size() > 0)
				{
					for(int i=0;i<listOfPackets.size();i++)
					{
						if(listOfPackets[i]->socket == nullptr)
						{
							sockets->send_mass(listOfPackets[i]->packet);
						}
						else
						{
							listOfPackets[i]->socket->send(listOfPackets[i]->packet);
						}
					}
					listOfPackets.clear();
				}
			}
		}
	}
}


Game* g;
void game()
{
	g->run();
}

int main(int argc, char* argv[])
{
	run = true;
	g = new Game();
	sockets = new Sockets();

	sf::Thread* thread  = new sf::Thread(&server_accept); // �����
	sf::Thread* thread2 = new sf::Thread(&game); // ����
	listener.listen(PORT); // ��������� ������� �� ����� 
	selector.add(listener);
	cout << "Create server v0.2  (07.10.2017)" << endl;
	
	thread->launch();
	thread2->launch();

	if(thread)  {  thread->wait(); delete thread;  }
	if(thread2) { thread2->wait(); delete thread2; }

	return 0;
}