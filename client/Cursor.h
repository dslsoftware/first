//Cursor.h
//v2.0 07.01.2017
#ifndef DSL__CURSOR__H
#define DSL__CURSOR__H

#include "stdafx.h"
#include "Design.h"

class Cursor
{
private:
	sf::Image   image;
	sf::Texture texture;
	sf::Sprite  sprite;
	std::vector<DesignCursor*> vecDesign; // ������ ��� 1 �������
	int num;
	void data(void);
public:
	bool isVisible;
public:
	Cursor(void);
	~Cursor(void);
	void setDesign(sf::String filename); 
	void setCursor(int number);
	void setPosition(float x, float y);
	void draw(sf::RenderWindow* window);
};

#endif DSL__CURSOR__H