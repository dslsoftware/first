#include "dslnet.h"

void coutPacket(sf::Packet packet)
{
	std::cout << std::endl << "<>packet begin</>" << std::endl;
	sf::Uint8 ch;
	for(int i=0; i<packet.getDataSize(); i++)
	{
		packet >> ch;
		std::cout << int(ch) << " ";
	}
	std::cout << std::endl << "<>packet end</>" << std::endl;
}

sf::Packet createPacket(sf::Uint16 code)
{
	sf::Packet packet;
	packet << code;
	return packet;
}
sf::Packet createPacket(sf::Uint16 code, int id)
{
	sf::Packet packet;
	packet << code << id;
	return packet;
}
sf::Packet createPacket(sf::Uint16 code, int id, std::string str1)
{
	sf::Packet packet;
	packet << code << id << str1;
	return packet;
}

void send_mass(sf::Packet packet, std::vector<sf::TcpSocket*> vec)
{
	for(int i=0; i<vec.size()-1; i++)
		vec[i]->send(packet);
}
//