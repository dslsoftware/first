//myMath.cpp
//v2.0 07.01.2017
#include "myMath.h"

sf::Color xColor(sf::Color color)
{
	color.r = 255 - color.r;
	color.g = 255 - color.g;
	color.b = 255 - color.b;
	//color.a = 255 - color.a;
	return color;
}
int myMin(int n1, int n2)
{
	if(n1 < n2) 
		return n1;
	else
		return n2;
}
// ���������� ������� ���� ����������� � �������
float myCos(float angle)
{
	return cos(angle*PI/180);
}
// ���������� ����� ���� ����������� � ��������
float mySin(float angle)
{
	return sin(angle*PI/180);
}
// ���������� ������ �����
float myMod(float number)
{
	if(number >= 0)
		return number;
	else
		return -1 * number;
}
int myMod(int number)
{
	if(number >= 0)
		return number;
	else
		return -1 * number;
}
// ���������� ��������� ����� ����� �� n1 �� n2
int myRand1(int n1, int n2)
{
  if(n1 > n2)
	{
    n1++;
    return n2 + rand()%(n1-n2);
  } 
	else 
	{
    n2++;
    return n1 + rand()%(n2-n1);
  }
}
// ���������� 1 ���� ������ �������� � percent (%) �������
int myRand2(float percent)
{
	int n = rand()%100;

	if(n < percent)
		return 1;

	return 0;
}
// ���������� ������� �����
float n2(float n)
{
	return n*n;
}
// ���������� ���������� ����� ����� �������
float distance(float  x1, float  y1, float  x2, float  y2)
{
	return sqrt(n2(x1-x2) + n2(y1-y2));
}

float distance2(sf::Vector2f pos1, sf::Vector2f pos2)
{
	return sqrt(n2(pos1.x-pos2.x) + n2(pos1.y-pos2.y));;
}
// ���������� ���� �� ���� ��������� ��������
float d_angle(float dx, float dy)
{
	float angle = 90 + atan2(dy,dx)*180/PI;
	if(angle <   0) angle += 360;
	if(angle > 360) angle -= 360;
	return angle;
}
// ���������� ���� ???
float d_angle(float x1, float y1, float x2, float y2)
{
	float angle = 90 + atan2(y2-y1,x2-x1)*180/PI;
	if(angle <   0) angle += 360;
	if(angle > 360) angle -= 360;
	return angle;
}
// a,b - ����� �� ���������� p - ����� ����������
float d_angle(sf::Vector2f pos1, sf::Vector2f pos2)
{
	float angle = 90 + atan2(pos2.y-pos1.y,pos2.x-pos1.x)*180/PI;
	if(angle <   0) angle += 360;
	if(angle > 360) angle -= 360;
	return angle;
}
float d_angle(sf::Vector2f a, sf::Vector2f b, sf::Vector2f p)
{
	float angleA = d_angle(p.x, p.y, a.x, a.y);
	float angleB = d_angle(p.x, p.y, b.x, b.y);
	if(angleA < angleB)
		return angleB - angleA;
	else
		return 360 - angleA + angleB;
}
// ���������� true ���� ��� ���������� �������������
bool HitTestPointCircle(float x1, float y1, float d1, float x2, float y2, float d2)
{
	if(myMod(x2-x1) <= myMod(d2+d1)/2)                     // ���� ������ �������� ��������� � ������ ��� ����� �������� ������ ����� ���������  
		if(myMod(y2-y1) <= myMod(d2+d1)/2)                   // ����������� �������� �� Y
			if(sqrt(n2(x2-x1) + n2(y2-y1)) <= myMod(d2+d1)/2)  // ���������� �� ��������� <= ��������� ���������
				return true;
			else
				return false;
		else 
			return false;
	else 
		return false;
}
sf::String intToString(int value)
{
	std::ostringstream str;
	str << value;
	return str.str();
}
// ���������� ������ ���� ��� �������
// ��������� ������ � �������
std::vector<sf::String> scanWords(sf::String str)
{
	std::vector<sf::String> vecStr;
	sf::String buff;

	for(int i=0; i<str.getSize(); i++)
		if(str[i] != ' ')
			buff += str[i];
		else
			if(buff != "")
			{
				vecStr.push_back(buff);
				buff = "";
			}
    vecStr.push_back(buff);

		return vecStr;
}
// ���������� ������ ������� �����
// ��������� ������ � ������ ���� � ����������� ����� (� ������� � ��������)
std::vector<sf::String> scanString(int w, std::vector<sf::String> vecWords_, sf::Text* text)
{
	std::vector<sf::String> vecStr;
	sf::String buff;
	sf::String buff2;

	for(int i=0; i<vecWords_.size(); i++)
	{
		buff = buff2;
		if(buff2 == "")
			buff2 += vecWords_[i];
		else
			buff2 += " " + vecWords_[i];
		text->setString(buff2); // ������ ������ ����� �������� ������ ������
		if(text->getLocalBounds().width > w)
		{// ���� ��� ���������� ����� ������ ����� ������ ������
			vecStr.push_back(buff); // ��������� � ������ ����� �����1 (�� �������� ���������� �����)
			buff = "";           // ������ �����1
			buff2 = vecWords_[i]; // � �����2 ���������� ��������� ����� (�.�. ������� �� ����. ������)
		}
	}
	vecStr.push_back(buff2);

	return vecStr;
}
//