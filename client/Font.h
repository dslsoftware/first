#pragma once
#include <SFML/Graphics.hpp>

class Font : public sf::Font
{
public:
	static Font* Instance()
	{
		static Font* font = new Font();
    return font;
	}
};