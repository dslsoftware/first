//Cursor.cpp
//v2.0 07.01.2017
#include "Cursor.h"

Cursor::Cursor(void)
{
	data();
}

Cursor::~Cursor(void)
{
}

void Cursor::data(void)
{
	vecDesign.reserve(1);
	DesignCursor* dc = new DesignCursor("images/Design/Cursor/flyff/flyff.png");
	vecDesign.push_back(dc);
	isVisible = false;
	num = -1;
	setCursor(0);
}

void Cursor::setDesign(sf::String filename)
{
	DesignCursor* dc = new DesignCursor(filename);
	vecDesign[0] = dc;
	num = -1;
	setCursor(0);
}

void Cursor::setCursor(int number)
{
	if(number != num)
	{
		num = number;
		image = vecDesign[0]->getImage(number);
		texture.loadFromImage(image);
		sprite.setTexture(texture);
		sprite.setTextureRect(sf::IntRect(0,0,image.getSize().x, image.getSize().y));
		if(number != 0 && number != 6)
			sprite.setOrigin(image.getSize().x/2, image.getSize().y/2);
		else
			sprite.setOrigin(0,0);
	}
}

void Cursor::setPosition(float x, float y)
{
	sprite.setPosition(x, y);
}

void Cursor::draw(sf::RenderWindow* window)
{
	if(isVisible)
		window->draw(sprite);
}